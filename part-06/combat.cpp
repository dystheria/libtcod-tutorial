#include "main.hpp"

Combat::Combat(float atk) :atk(atk) {}

void Combat::attack(std::shared_ptr<Ent>attacker, std::shared_ptr<Ent>target) {
	if (target->mortal) {
		if (!target->mortal->isDead()) {
			float dmg(atk - target->mortal->def);
			if (dmg > 0.0) {
				printf("The %s hits the %s for %g damage!\n", attacker->name, target->name, dmg);
				// hit successfully does damage
				target->mortal->takeDamage(target, atk);
				// resolve damage
			} else {
				printf("The %s is too weak to harm the %s!\n", attacker->name, target->name);
				// hit does no damage due to not being strong enough
}}}	else {
		printf("The %s is impervious to damage.\n", target->name);
		// The target isn't mortal and has no health, so any attack cannot possibly harm it
}}