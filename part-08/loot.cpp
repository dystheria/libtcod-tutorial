#include "main.hpp"

bool Loot::collect(std::shared_ptr<Ent> owner, std::shared_ptr<Ent> wearer) {
	if (wearer->container && wearer->container->add(owner)) {
		auto it = find(engine.entL.begin(), engine.entL.end(), owner);
		engine.entL.erase(it);
		return true;
	}
	return false;
}

bool Loot::use(std::shared_ptr<Ent> owner, std::shared_ptr<Ent> wearer) {
	if (wearer->container) {
		wearer->container->del(owner);
		return true;
	}
	return false;
}

Healer::Healer(float amt) : amt(amt) {}

bool Healer::use(std::shared_ptr<Ent> owner, std::shared_ptr<Ent> wearer) {
	if (wearer->mortal) {
		float amtHealed(wearer->mortal->heal(amt));
		if (amtHealed > 0) { return Loot::use(owner, wearer); }
	}
	return false;
}