** Part-1, the walking @ **

Very similar to the Roguebasin tutorial with only one real difference
Instead of putting everything in to one "main.cpp" file, I've anticipated the need for a central header file.

Both source files are heavily commented, mostly as a means of solidifying what I'm learning in my own mind.

## our first header file and what it does.

	main.hpp:
		#pragma once
		
		#include "libtcod.hpp"

A nice simple header file to get us started. This "main.hpp" header file won't contain any detailed code or instructions, instead it will act as our central handler for including other headers.
This will ensure that the order of headers will always be well structured, and we don't have to duplicate effort in our source files by having to call multiple header files all the time.

The "pragma once" command is actually a non-standard compiler instruction, but most (if not all) compilers support it. "pragma once" tells the compiler to only load the header file once.
As a by-product, each include call will only be carried out once, so there is no risk of the compiler trying to load an include twice and spitting up errors at us that it looks like we've tried to declare classes twice or such.
Good pre-processor and compiler practices are kind of beyond the scope of this tutorial, but if you're serious about programming it's worth learning some good habits.

We have but one include at this point, and that's the libtcod header.

## our first source file and what it does.

	main.cpp:
		#include "main.hpp"
	
		int main() {
			int playerx = 40, playery = 25;
			TCODConsole::initRoot(80, 50, "ALT|A libtcod tutorial v0.1", false);
			while (!TCODConsole::isWindowClosed()) {
				TCOD_key_t key;
				TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS, &key, NULL);
				switch (key.vk) {
					case TCODK_UP: playery--; break; 
					case TCODK_DOWN: playery++; break; 
					case TCODK_LEFT: playerx--; break;
					case TCODK_RIGHT: playerx++; break;
					default:break;
				}
				TCODConsole::root->clear();
				TCODConsole::root->putChar(playerx, playery, '@');
				TCODConsole::flush();
			}
		}

If you've already snuck a look at the main.cpp provided with this part, you'll see it's already filled with comments on what each section does, but I'll break down the code anyway for posterity.

`#include "main.hpp"` This includes our main header, and by proxy the libtcod.hpp header.

`int main() {` we open our main routine.

`int playerx = 40, playery = 25;` declare two integer variables, playerx and playery, to represent the position of the player icon, and to save time we set the values in the declaration.
This doesn't have to be written this way, we could declare the variables and initialize them with values in the following line, but it seems unnecessary when we can initialize them at declaration.

`TCODConsole::initRoot(width, height, title, fullscreenBool);` This command initializes our root console that we'll use to display the game world, I've changed the values in this code example to explain what each value is.

`while (!TCODConsole::isWindowClosed()) {` We want our main routine to repeat while the game is running, so we open a loop that only runs while the TCOD Console window is not closed.

Next up we establish a switch case for the directional movement of our player, we do this by having each key press alter the x or y value of the "player" icon.

		//set a user defined variable "key" to a type defined in libtco, "TCOD_key_t"
		TCOD_key_t key;
		//wait for any key press
		TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS, &key, NULL);
		//handle the x and y player position variables with a switch
		switch (key.vk) {
			case TCODK_UP: playery--; break; //arrow key up, subtract 1 from playery
			case TCODK_DOWN: playery++; break; //arrow key down, add 1 to playery
			case TCODK_LEFT: playerx--; break; //etcetera for x
			case TCODK_RIGHT: playerx++; break; //and again...
			//break in the case of any other press
			default:break;
		}

Whenever this switch is set, we need to clear, update, and "flush" the console so that any changes are visible in the game world.

		//clear the console we initialized
		TCODConsole::root->clear();
		//slap the '@' symbol for our player on it at "playerx","playery" position
		TCODConsole::root->putChar(playerx, playery, '@');
		//flush the console, effectively drawing it out visibly for us, the player.
		TCODConsole::flush();
		}
	}

And that's it, save your code, compile, link and test, assuming I haven't provided shoddy guidance, you should now have a white **@** symbol that can be moved with the cursor keys.