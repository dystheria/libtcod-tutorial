**complete roguelike in C++17 && libtcod-1.7.0-x86_64-msvc tutorial**

This repository aims to be a modernisation of the [Roguebasin tutorial](http://www.roguebasin.com/index.php?title=Complete_roguelike_tutorial_using_C%2B%2B_and_libtcod_-_part_1:_setting_up)
Even though the Roguebasin article suggests that if you're a beginner you avoid using Visual Studio, I can tell you that I (a complete beginner) have managed to follow along pretty well.

I will upload my source as I go, providing comment where I can on areas where I have differentiated from the Roguebasin tutorial.

**Part-0.md** is currently available for anyone having trouble getting MS Visual Studio to play well with libtcod 1.7.0.

And check out [R/RogueLikeDev](https://www.reddit.com/r/roguelikedev/) on reddit for advice, guidance and pointers.