#include "main.hpp"
Ent::Ent(int x, int y, int ch, const char * name, const TCODColor &col) : 
	x(x),y(y),ch(ch),name(name),col(col),blocks(true),
	combat(nullptr),mortal(nullptr),ai(nullptr),loot(nullptr),container(nullptr) {}

void Ent::update(std::shared_ptr<Ent> owner) { if (ai) ai->update(owner); }

void Ent::render() const {
	TCODConsole::root->setChar(x, y, ch);
	TCODConsole::root->setCharForeground(x, y, col);
}