# Creating Procedural Dungeons
So we've got the basics down of having a walking **@** symbol on a simple map of tiles, and tiles can be either walls, or floors. It'd be tedious to create every room manually, and even worse than that, it'd be uninspiring and boring to play.

** PREFACE: Ignore any coloring of code blocks that the bitbucket markdown has tried to implement, it's crazy and I haven't a clue how to turn that crud off.**

## map.hpp
We're going to use libtcod's BSP tree function [TCODBsp::TCODBSP](http://roguecentral.org/doryen/data/libtcod/doc/1.5.1/html2/bsp_init.html?c=false&cpp=true&cs=false&py=false&lua=false) to map out rooms for us, but in order to prevent having to write some obtuse script to put walls around our rooms, we'll change the basic function of the map generator so that the map begins with all tiles being walls.

So, change the below section of map.hpp:

	map.hpp
		struct Tile { bool canWalk; Tile() : canWalk(true) {} };

To read `canWalk(false)` instead, like so:

	map.hpp
		struct Tile { bool canWalk; Tile() : canWalk(false) {} };

We'll no longer be manually setting walls, but we will need to dig out rooms instead, so lets replace the `void setWall(x,y)` with a friend class for the TCODBsp, a dig function to dig out tunnels that link rooms, and a create room function to dig out our rooms.

	map.hpp
		protected:
			Tile *tiles;
				friend class bspLst;
				void dig(int x1, int y1, int x2, int y2);
				void cRoom(bool first, int x1, int y1, int x2, int y2);
		};

`bspLst` is going to be a listener that will be private to the Map class in this tutorial implementation because we only want the map to control making rooms, `dig` and `cRoom` will be responsible for digging tunnels and creating our rooms in our implementation.

## map.cpp
We need to have static constants that control the possible maximum and possible minimum room size, I'm a fan of [Camel case](https://en.wikipedia.org/wiki/Camel_case) so I'm going to use `rsMIN` and `rsMAX` for our Room Size min and max.
These will be static and therefore only have scope within the map.cpp module because ultimately, the possible min/max size of any room will only be referenced by our map creator functions.

Speaking of map creator functions, we'll need to create the implementations of `dig` and `cRoom`, as well as the handler for the BSP listener `bspLst` so. This will be a tad bulkier than before but try to bear with the code:

	map.cpp
		#include "main.hpp"
		
		static const int rsMIN = 6, rsMAX = 12;
		
		class bspLst : public ITCODBspCallback {
		private:
			Map &dungeon; int rNum; int lastx,lasty;
		public:
			bspLst(Map &dungeon) : dungeon(dungeon), rNum(0) {}
			bool visitNode(TCODBsp *node, void *userData) {
				if (node->isLeaf() {
					int x,y,w,h;
					TCODRandom *rng=TCODRandom::getInstance();
					w=rng->getInt(rsMIN,node->w-2);
					h=rng->getInt(rsMIN,node->h-2);
					x=rng->getInt(node->x+1, node->x+node->w-w-1);
					y=rng->getInt(node->y+1, node->y+node->h-h-1);
					dungeon.cRoom(rNum == 0, x,y,x+w-1,y+h-1);
					if (roomNum != 0) {
						dungeon.dig(lastx,lasty,x+w/2,lasty);
						dungeon.dig(x+w/2,lasty,x+w/2,y+h/2);
					}
					lastx=x+w/2; lasty=y+h/2; rNum++;
				}
				return true;
		} };

Now, there is a fair bit of Math going on here, but it should all make sense.
In order, here are the action's we've just taken with this implementation:

1. declare and initialize our `rsMIN` and `rsMAX` constants
2. create the bspLst declaration
3. establish the private parameters it will handle:
	+ the dungeon map `Map &dungeon`, an integer for room number `int rNum`, and two integers to hold the center position of the last room created `int lastx,lasty`.
4. define the constructor function in-line with the class declaration for bspList.
5. provide a function to visit nodes of the BSP tree.
6. begin our room creation loop if, and only if, we are in a BSP leaf.
7. make liberal use of TCODRandom and its ability to provide random numbers so that we can populate our:
	+ width, `w`, as a random number between the minimum room size and the width of the node minus 2.
	+ height, `h`, as above, but using the height of the node minus 2.
	+ starting x position, `x`, as a random number between the start x of the node plus 1 and the width of the node, minus our random width, minus 1.
	+ starting y position, `y`, as above, but using the height of the node, minus our random height, minus 1.
		+ the "plus 1", "minus 2", and "minus 1" is done to compensate for the room walls, ensuring that no room will go beyond the bounds of the dungeon map should a room be placed right on the edge of the map.
8. if this is not the first room, dig a tunnel back to the room before us.
9. set the co-ordinates for the sent of the last room, and increase our room number value by 1.
10. lastly, return true, and move on through the BSP tree.

Once it's broken down, it's simpler than it first seems.

#### generating the actual dungeon map.
So we now have our function that will create rooms for us but we haven't create the `cRoom` implementation, the dig function to link them, *or* updated the map constructor to use our new tools.

I'll begin with the Map constructor

	map.cpp
		Map::Map(int w, int h) : w(w), h(h) {
			tiles = new Tile[w*h];
			TCODBsp bsp(0,0,w,h);
			bsp.splitRecursive(NULL, 8, rsMIN, rsMAX, 1.5f, 1.5f);
			bspLst listener(*this);
			bsp.traveseInvertedLevelOrder(&listener, NULL);
		}

So now we're telling the Map::Map constructor to invoke the TCODBsp function, providing it with a start and end point, and also telling it to split our dungeon chunks recursively, we establish a listener for the process, passing our map through to it, and inform the bsp algorithm to traverse the tree as it goes so that we actual move through each node and leaf.

If you're big on dungeon creation algorithms and want to know how to manipulate this further to get more interesting layouts, I suggest reading up on the [BSP toolkit](http://roguecentral.org/doryen/data/libtcod/doc/1.5.1/html2/bsp.html?c=false&cpp=true&cs=false&py=false&lua=false)

#### digging and room creation

	map.cpp
		void Map::dig(int x1, int y1, int x2, int y2) {
			if (x2 < x1) { int tmp = x2; x2 = x1; x1 = tmp; }
			if (y2 < y1) { int tmp = y2; y2 = y1; y1 = tmp; }
			for (int tx = x1; tx <= x2; tx++) {
				for (int ty = y1; ty <= y2; ty++) {
					tiles[tx + ty * w].canWalk = true;
		} }	}
		
		void Map::cRoom(bool first, int x1, int y1, int x2, int y2) {
			dig(x1, y1, x2, y2);
			int cx((x1 + x2) / 2), cy((y1 + y2) / 2);
			if (first) { 
				engine.player->x = cx; engine.player->y = cy;
			} else {
				TCODRandom *rng = TCODRandom::getInstance();
				if (rng->getInt(0, 3) == 0) { engine.entL.push(new Ent(cx, cy, '@', TCODColor::yellow)); }
		} }

And here we have our `Map::dig` and `Map::cRoom` functions respectively.
This all uses functions, logic and math that we've already implemented or used prior to this point. in our `cRoom` function, we have created an else case for any room that is not the first room which will pick a random number between 0 and 3, if the number is 0, we create an NPC, because of this we don't need to create a static NPC in our engine.cpp, but we'll clean that up before we finish the lesson.

Two notes to be aware of in this section

For the dig function:
`if(x2<x1) {int tmp = x2; x2 = x1; x1 = tmp; }` this little piece of code tests if our x2 is smaller than our x1, and flips them if the condition is true, this ensures that we're never trying to dig out negative tunnels. Try commenting these inverter conditions out and see what happens.

For the cRoom function:
`int cx((x1+x2)/2),cy((y1+y2)/2);` This is just another way of typing `int cx = (x1+x2)/2; int cy = (y1+y2)/2;` you don't have to use an equals to initialize a variable when declaring it, and can use the format of `type var(value)`
On a more personal note, I really dislike needless repetition, and I'm always looking for ways to make my code more efficient and minimize repetition, especially where math is involved. By creating the values cx and cy pre-emptively we save ourselves from having to repeatedly type ((x1+x2)/2) or ((y1+y2)/2), any mathematical function you have to write more than once is a mathematical function written one time too many in my book. So always try to look for areas where you can pre-emptively declare and initialize a value if you know that value is going to be used several times.

#### tiny clean up action
We're still in map.cpp, but we have a function floating around that no longer has a class declaration and is no longer needed as we're not going to be using it from here on out... so...

	map.cpp
		void Map::setWall(int x, int y) { tiles[x + y * w].canWalk = false; }

we don't need this line anymore, so delete it! Or comment it out with // at the start of the line and pop a note on the end to the tune of "we aren't using this anymore.", the choice is yours, it's your code now.

## engine.cpp
Remember we have to clean up that static NPC that we created in the engine.cpp, open the file up and delete the `entL.push(new` line so that your engine constructor looks like this:

	engine.cpp
			Engine::Engine() {
			TCODConsole::initRoot(80, 50, "ALT|A Libtcod Tutorial v0.3", false);
			player = new Ent(40, 25, '@', TCODColor::white); entL.push(player);
			dungeon = new Map(80, 50);
		}

And that's us finished once again, save your files, rebuild your solution, mess around with your new fancy dungeons. You could even try playing with the numbers a little to see what shapes and densities you can create.