class Ent {
public:
	// base entity properties
	int x, y, ch; TCODColor col;
	const char * name;
	bool blocks;

	// entity feature pointers
	std::shared_ptr<Combat> combat;
	std::shared_ptr<Mortal> mortal;
	std::shared_ptr<Ai> ai;
	std::shared_ptr<Loot> loot;
	std::shared_ptr<Container> container;

	// base entity functions
	Ent(int x, int y, int ch, const char * name, const TCODColor &col);
	void update(std::shared_ptr<Ent> owner);
	void render() const;
};