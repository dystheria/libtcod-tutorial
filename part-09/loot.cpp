#include "main.hpp"

bool Loot::collect(std::shared_ptr<Ent> owner, std::shared_ptr<Ent> bearer) {
	if (bearer->container && bearer->container->add(owner)) {
		auto it = find(engine.entL.begin(), engine.entL.end(), owner);
		engine.entL.erase(it);
		return true;
	}
	return false;
}

bool Loot::use(std::shared_ptr<Ent> owner, std::shared_ptr<Ent> bearer) {
	if (bearer->container) {
		bearer->container->del(owner);
		return true;
	}
	return false;
}

void Loot::drop(std::shared_ptr<Ent> owner, std::shared_ptr<Ent> bearer) {
	if (bearer->container) {
		bearer->container->del(owner);
		engine.entL.push_back(owner);
		owner->x = bearer->x; owner->y = bearer->y;
		if (bearer == engine.player) {
			engine.gui->message(TCODColor::lightGrey, "You have dropped the %s.", owner->name);
		} else {
			engine.gui->message(TCODColor::lightGrey, "The %s has dropped a %s.", bearer->name, owner->name);
} } }

Healer::Healer(float amt) : amt(amt) {}
eBolt::eBolt(float range, float dmg) : range(range), dmg(dmg) {}
fBall::fBall(float range, float dmg) : eBolt(range, dmg) {}
Confuser::Confuser(int nbTurns, float range) : nbTurns(nbTurns), range(range) {}

bool Healer::use(std::shared_ptr<Ent> owner, std::shared_ptr<Ent> bearer) {
	if (bearer->mortal) {
		float amtHealed(bearer->mortal->heal(amt));
		if (amtHealed > 0) { return Loot::use(owner, bearer); }
	}
	return false;
}

bool eBolt::use(std::shared_ptr<Ent> owner, std::shared_ptr<Ent> bearer) {
	std::shared_ptr<Ent> closestMob = (engine.getClosestMob(bearer->x,bearer->y,range));
	if (!closestMob) { engine.gui->message(TCODColor::lightGrey, "No enemy is close enough to strike."); return false; }
	engine.gui->message(TCODColor::azure, "An azure bolt splits the air with a crack");
	engine.gui->message(TCODColor::azure, "The %s is struck for %g damage.", closestMob->name, dmg);
	closestMob->mortal->takeDamage(closestMob, dmg);
	return Loot::use(owner, bearer);
}

bool fBall::use(std::shared_ptr<Ent> owner, std::shared_ptr<Ent> bearer) {
	engine.gui->message(TCODColor::cyan, "Left-click to cast fireball,\nor right-click to cancel.");
	int x, y;
	if (!engine.pickTile(&x, &y, 8)) { return false; } // there is a magic number here, not a good practice but I plan on fixing that later.
	engine.gui->message(TCODColor::flame , "The fireball explodes burning everything in a %g radius!", range);
	for (auto &ent : engine.entL) {
		if (ent->mortal) {
			if (!ent->mortal->isDead() && ent->getDist(x,y)<=range) {
				engine.gui->message(TCODColor::flame, "The %s is burned by the fireball for %g damage.", ent->name, dmg);
				ent->mortal->takeDamage(ent, dmg);
	} } }
	return Loot::use(owner, bearer);
}

bool Confuser::use(std::shared_ptr<Ent> owner, std::shared_ptr<Ent> bearer) {
	engine.gui->message(TCODColor::green, "Left-click an enemy to cast confuse,\nor right-click to cancel.");
	int x, y;
	std::shared_ptr<Ai> confusedAi;
	if (!engine.pickTile(&x, &y, range)) { return false; }
	std::shared_ptr<Ent> targetMob = engine.getMob(x, y);
	confusedAi = std::make_shared<ConfusedMobAi>(nbTurns, targetMob->ai);
	targetMob->ai = confusedAi;
	engine.gui->message(TCODColor::lightGreen, "The %s begins to wander aimlessly", targetMob->name);
	return Loot::use(owner, bearer);
}