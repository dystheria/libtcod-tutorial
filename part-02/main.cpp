#include "main.hpp"

Engine engine;

int main() {
	while (!TCODConsole::isWindowClosed()) {
		engine.update(); engine.render();
		TCODConsole::flush();
	}
}