# Part-07 A self contained GUI

The roguebasin tutorial for this section can be found [here](http://www.roguebasin.com/index.php?title=Complete_roguelike_tutorial_using_C%2B%2B_and_libtcod_-_part_7:_the_GUI).

Check out [r/RogueLikeDev](https://www.reddit.com/r/roguelikedev/) for advice, guidance, tips and discussion.

## Why we can't have nice strings

Part 7 doesn't deviate much from the roguebasin tutorial because we're using libtcod as our console interface library which means we're very much restricted to the expectations of the libtcod library. I would have liked to have completed done away with the use of `const char *` raw pointers the `<cstdio>` header and used `<strings>` instead, however `TCODConsole::print` explicitly only accepts `const char *<printf-like format string>` and does not accept `std::string`.

However, This is just one of the trade-offs you have to accept for the immediate functionality that libtcod provides.

## Declaring the GUI features

### gui.hpp

We need to create the Gui class with a public constructor and deconstructor as well as a void render function and a void message function, the message function will be variadic, if we could break away from the use of `const char *` variable types we wouldn't use variadic functions at all as `<strings>` have plenty of features to allow us to make on-the-fly variable replacements within string variables, *with that said* we're stuck with printf-like strings so we need to us a variadic function.

For reference a variadic function is defined as a function of infinite arity. What this means is that any number of arguments can be passed to it after the declaration of its primary arguments.

We make the function variadic by giving it a final argument of `...`, but the first two arguments we'll need for the message function are a const reference to TCODColor, and a const char pointer to the text, so the finished article should look like this `void message(const TCODColor &col,const char *text, ...);`

The Gui class will also require several protected elements, first we need a raw TCODConsole pointer, `TCODConsole *con;`, next we're going to create a struct for handling Messages, appropriately called "Message", this struct object will need a char pointer for the text, a col variable for the TCODColor of the text, and it's own constructor and deconstructor. You should be able to guess that the constructor will accept two arguments, `Message(const char *text, const TCODColor &col);`

Next up we want to create a vector of shared pointers to our messages, we're going to call this vector "log". The roguebasin article uses TCODList for this, but TCODList is often an unnecessary container and `<vector>` allows us a greater deal of flexibiilty and control in a more elegant method. (It also has the bonus of being able to work with the STL and all of the algorithms within it.)

Lastly, we have two void functions, one to render the health bar, named "renderBar", which will require an extensive array of arguments so I'm just going to provide the source-code verbatim:

	gui.hpp
		void renderBar(int x, int y, int width, const char *name, 
		float value, float maxValue, const TCODColor &barColor, 
		const TCODColor &backColor);

The final void function will be `void renderMouseLook();` which requires no arguments and is quite self explanatory.

### gui.cpp

Include our `main.hpp` header as always, and then we need to define four `static const int pHeight(7), bWidth(20),msgX(bWidth+2), msgHeight(pHeight-1);`. In order, these are the gui *panel height*, the *bar width*, the *message x position*, and the *message log height*.

Our Gui constructor will simply initialise `con` as a new TCODConsole, it will be `engine.sW` wide and `pHeight` tall. the Gui deconstructor will `delete con;` and `log.clear();`. All straight forward clean-up actions.

Rendering the Gui looks quite involved at first glance, but is relatively straight forward, however, the `Gui::render()` function makes a call to the `Gui::renderbar()` function *and* to the `renderMouseLook()` function, we're still going to define the Gui render first though.

	gui.cpp
		void Gui::render() {
			// set the gui console background color and clear it.
			con->setDefaultBackground(TCODColor::black);
			con->clear();
			
			// render a bar at x,y position 1,1 that is bWidth wide, called "HP", draws the dynamic value from the player hp and the max value from the player maxhp, and give it the colors lightRed and darkerRed
			renderBar(1, 1, bWidth, "HP", engine.player->mortal->hp, engine.player->mortal->MaxHp,
				TCODColor::lightRed, TCODColor::darkerRed);
			
			// the integer y will be responsible for our message position on the console
			// cCoef is short for "color Coefficient" and will make text look darker
			int y(1); float cCoef(0.4f);
			
			// we do love our ranged fors.
			for (auto &msg : log) {
				// multiply the foreground color by the color coefficient, then print the text to the console at position msgX,y before incrementing y.
				
				con->setDefaultForeground(msg->col * cCoef); con->print(msgX, y, msg->text); y++;
				
				// test to see if the color coefficient is less than 1.0f, if it is, add 0.3f to it
				if (cCoef < 1.0f) { cCoef += 0.3f; }
				// we do this BEFORE the ranged-for loops, meaning so long as the cCoef is less than 1.0f, it will continue to be made brighter with each message printed.
			}
			
			//render mouse look, just in case we're hovering over anything of interest
			renderMouseLook();
			
			//blit the console to the root console.
			TCODConsole::blit(con, 0, 0, engine.sW, pHeight,
				TCODConsole::root, 0, engine.sH - pHeight);
		}

That was a hoot, but now we have to define the renderBar function, this is basically just drawing a rectangle and then using a spot of math and a test condition to dynamically update the bar width depending on how much health the player has left. Here is the full source for `Gui::renderBar()` it's pretty self explanatory so I've not provided comments.

	gui.cpp
		void Gui::renderBar(int x, int y, int width, const char * name,
			float value, float maxValue, const TCODColor &barColor,
			const TCODColor &backColor) {
			
			con->setDefaultBackground(backColor);
			con->rect(x, y, width, 1, false, TCOD_BKGND_SET);
			
			int barWidth = (int)(value / maxValue * width);
			if (barWidth > 0) {
				con->setDefaultBackground(barColor);
				con->rect(x, y, barWidth, 1, false, TCOD_BKGND_SET);
			}
			con->setDefaultForeground(TCODColor::white);
			con->printEx(x + width / 2, y, TCOD_BKGND_NONE, TCOD_CENTER,
				"%s : %g/%g", name, value, maxValue);
		}

... aaaand now, we need to render the mouse look. For this function we're basically implementing the function to utilize libtcod's mouse input functionality, obviously we don't have the back-end for this in place at present but we already know how to have the game return details for the tile we're standing on so the concept is pretty straight forward:

+ we only want it to render info for what's in our field of view.
+ we'll do the usual loop of entities to check what is actually on the tile in question.
+ and we need a means of passing the entity names through to a message for the Gui.

The method presented by roguebasin uses a char variable and the `strcat` function to concatenate the entities on any tile together in a list, it's simple and it's effective so there's no point in us reinventing the wheel here.

	gui.cpp
		void Gui::renderMouseLook() {
			if (!engine.dungeon->isInFov(engine.mouse.cx, engine.mouse.cy)) { return; }
			char buf[128] = ""; bool first = true;
			for (auto &ent : engine.entL) {
				if (ent->x == engine.mouse.cx && ent->y == engine.mouse.cy) {
					if (!first) { strcat_s(buf, ", "); }
					else { first = false; }
					strcat_s(buf, ent->name);
				}
			}
			con->setDefaultForeground(TCODColor::lightGrey); con->print(1, 0, buf);
		}

### Message handling

The `Gui::Message::Message` constructor will pass the established arguments in the gui.hpp, and then initialise them via initialisation list, `text` will actually be initialised as `text(_strdup(text));` but col will be a standard self referential `col(col)`.

The deconstructor will only need to deallocate the text const char pointer, this is done with `free(text);`.

the `void Gui::message` function is a tad more complex because of it's variadic nature. We also need to add the `<cstdarg>` header to our headerfile in order to implement these functions, but we'll end the lesson with a quick sanity check.

We're going to create a variadic list, a char buffer, and write variable strings to the list, and then we're going to define what we want to dynamically replace within our strings. The code also takes advantage of the fact that a c style string terminates in a `\0`.

	gui.cpp
		void Gui::message(const TCODColor &col, const char * text, ...) {
			va_list ap; char buf[128];
			va_start(ap, text); vsprintf_s(buf, text, ap); va_end(ap);
			
			char *lineBegin = buf; char *lineEnd;
			do {
				if (size(log) == msgHeight){log.erase(begin(log));}
				lineEnd = strchr(lineBegin, '\n');
				if (lineEnd) { *lineEnd = '\0'; }
				std::shared_ptr<Message> msg = std::make_shared<Message>(lineBegin, col);
				log.push_back(msg); lineBegin = lineEnd + 1;
			} while (lineEnd);
		}

The utilisation of both vectors and shared pointers means we have a very succinct test condition to keep the message log small, and our messages are automatically cleaned up for us as they go out of scope, it's not as elegant as it could be if we had a means of dropping the c style strings entirely but it's better than no improvement at all.

## updating the engine

We still need to round out the mouse-look functionality:

1. add `TCOD_mouse_t mouse;` to the engine header file.
2. update the `TCODSystem::checkForEvent` to include `TCOD_EVENT_MOUSE` and the `&mouse` field.
	+ `TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS|TCOD_EVENT_MOUSE,&latKey,&mouse);`
3. update the `Map::isInFov` so that it can handle map values that are out of bounds.
	+ `if(x<0||x>=w||y<0||y>=h){return false;}`

- and that's it, coupled with the `Gui::renderMouseLook()` function, hovering over a non-empty tile will reveal information about it on the Gui now.

### sanity check

Don't forget to update your header file with the `<cstdarg>` standard header. We can also remove `<iostream>` and `<string>` from the header too as we aren't making use of them anymore.

	main.hpp
		#pragma once
		#include <iostream> //remove this
		#include <vector>
		#include <memory>
		#include <random>
		#include <functional>
		#include <string> //and remove this
		#include <utility>
		#include <cstdarg> //add this
		#include "libtcod.hpp"
		class Ent;
		#include "mortal.hpp"
		#include "combat.hpp"
		#include "ai.hpp"
		#include "entity.hpp"
		#include "map.hpp"
		#include "gui.hpp"
		#include "engine.hpp"

Now see if it builds and enjoy your shiny new gui!

If it throws up errors all over the joint and then falls over screaming, feel free to tell me how awful the tutorial is, preferably in a constructive manner by letting me know what errors you got when trying to compile or what errors are thrown at runtime.