#include "main.hpp"

Engine::Engine(int sW, int sH) : gameState(START), fovRad(10),sW(sW),sH(sH) {
	TCODConsole::initRoot(sW, sH, "ALT|A Libtcod Tutorial v0.6", false);
	player = std::make_shared<Ent>(1, 1, '@', "player", TCODColor::white);
	player->mortal = std::make_shared<pcMortal>(30, 2, "your lifeless corpse");
	player->combat = std::make_shared<Combat>(5);
	player->ai = std::make_shared<PlayerAi>();
	entL.push_back(player);

	dungeon = std::make_unique<Map>(80, 45);
}

Engine::~Engine() { entL.clear(); }

void Engine::update() {
	if (gameState == START) dungeon->computeFov();
	gameState = IDLE;
	TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS, &lastKey, NULL);
	player->update(player);
	if (gameState == TURN) {
		for (auto &ent : entL){if(ent->mortal){if(ent->name!="player"&&!ent->mortal->isDead()){ent->update(ent);}
}}}}

void Engine::render() { 
	TCODConsole::root->clear(); dungeon->render(); 
	for (auto &ent : entL){if(ent->mortal){if(ent->mortal->isDead()){if(dungeon->isInFov(ent->x,ent->y)){ent->render();}}}}
	for (auto &ent : entL){if(ent->mortal){if(!ent->mortal->isDead()){if(dungeon->isInFov(ent->x,ent->y)){ent->render();}}}}
	player->render();
	TCODConsole::root->print(1, sH - 2, "HP : %d:%d", (int)player->mortal->hp, (int)player->mortal->MaxHp);
}