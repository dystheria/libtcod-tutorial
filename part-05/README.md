# Part-05 Gromlins and Hobgobbos

The roguebasin tutorial for this section can be found [here](http://www.roguebasin.com/index.php?title=Complete_roguelike_tutorial_using_C%2B%2B_and_libtcod_-_part_5:_preparing_for_combat).

If you're not aware of it (and you really should be by now), check out [r/RogueLikeDev](https://www.reddit.com/r/roguelikedev/) for advice, guidance, tips and discussion.

More refactoring, reimplementation of the canWalk bool (as a property of entities this time round, rather than just a tile property), improving the entity class with names and random placement of monsters.
I'm sure this won't go hideously wrong at all.

## canWalk and Monsters.

### map.hpp

We're re-implementing a canWalk bool in map.hpp, as with most state check bools we want a `const` condition that checks the x and y of an entity, so `bool canWalk(int x, int y) const;` is all we need.
While we're in map.hpp it'd make sense to create the addMonster function, which will be a simple `void addMonster(int x, int y);`.

### map.cpp

We'll need a new `static const int` that dictates the maximum number of monsters per room, I've named mine **rMonstersMax** and initialized it with a value of 3.

Then we're going to create the canWalk implementation, the structure of the bool is a two stage condition check:

1. If the tile we're checking is a wall, return false.
2. If the tile we're checking is occupied by an entity, return false.

In all other scenarios (for the time being) we'll return true.

Here is my implementation if you don't want to work it out yourself:

	map.cpp
		bool Map::canWalk(int x, int y) const {
			if (isWall(x, y)) { return false; }
			for (auto &ent : engine.entL) {	if (ent->x == x && ent->y == y) { return false; } }
		return true;
		}

Next up we've got to create our `void Map::addMonster` function.
My own implementation will create a variable called `mDice` to hold the "random number roll" generated from TCODRandom.
The proposed implementation on roguebasin, rather unfortunately, uses a raw pointer which we are trying to move away from, I don't think it would be good practice to use this implementation, but I'm going to provide what the implementation would look like.

	example implementation
		void Map::addMonster(int x, int y) {
			TCODRandom *rng = TCODRandom::getInstance();
			int mDice(rng->getInt(0, 100));
			if (mDice < 80) { engine.entL.emplace_back(std::make_shared<Ent>(x, y, 'g', "Gromlin", TCODColor::desaturatedGreen)); }
			else { engine.entL.emplace_back(std::make_shared<Ent>(x, y, 'h', "Hobgobbo", TCODColor::darkOrange)); }
		}

The way I am going to achieve the same results is with use of the standard C++ header `<random>` by providing a random seed, declaring a random number generator with this seed to simulate the mechanic of rolling a dice, declaring and initializing the number distribution of the dice, and then lastly binding these two objects through a new variable that can be called to provide a random number on our expected dice range.

Below is my full implementation but keep in mind that using this method *requires we add `#include <random>` and `#include<functional>` to our main.hpp header*.

	map.cpp
		void Map::addMonster(int x, int y) {
			std::random_device seed;
			std::default_random_engine dRoll(seed());
			std::uniform_int_distribution<int> d100(1, 100);
			auto mDice = std::bind(d100, dRoll);
			if (mDice() < 80) { engine.entL.emplace_back(std::make_shared<Ent>(x, y, 'g', "Gromlin", TCODColor::desaturatedGreen)); }
			else { engine.entL.emplace_back(std::make_shared<Ent>(x, y, 'h', "Hobgobbo", TCODColor::darkOrange)); }
		}

### fixing an error
Way back when we created the `Map::Map()` constructor, we made a small mistake with the `bsp.splitRecursive` function, the third argument for it is the minimum horizontal size, it should be the size or our room maximum, `rsMAX`, we set it to `rsMIN` instead, resulting in some overlap with our room generation, it's entirely up to you if you fix this, replacing it with `rsMAX` will result in cleaner looking dungeon layouts without room overlap.
		
## Improving our Entities.
You may have noticed that our new shared entity pointers have one field more than they should, which we have used to hold the "name" of the entity. We'll now update the entity class and implementation so that it can make use of this.

### entity.hpp
The tutorial uses some C style code rather than C++ to create and store the name of entities, we're trying to learn C++ and not C, so I've opted to change out the use of `const char *name` for `const std::string name`, as you may have guessed, if we want to work with strings this means we have another header include to add to the main.hpp, don't worry, there will be a reminder at the bottom of the full main header.

So, go ahead and declare a `const std::string name;` in the Ent class, and update the Ent variable so that it includes our new "name" field like so: `Ent(int x, int y, const std::string name, const TCODColor &col);`.

We will need a bool to check whether an entity is moving or attacking, and we're going to want all entities to be able to `update();` in the game engine. So create `bool moveOrAttack(int x, int y);` and `void update();` for us to flesh out in a moment.

### entity.cpp
The above change to the Ent class means we need to update our constructor to include the new field, and our initiliazer list needs `name(name)` added to the fray.

We're going to implement our `Ent::moveOrAttack` now, at present this will primarily confirm whether the player entity can move or attack. Move in the case of their path not being obstructed, and attack if their path is obstructed by an enemy entity.

The implementat should look something like this:

	entity.cpp
		bool Ent::moveOrAttack(int x, int y) {
			if (engine.dungeon->isWall(x, y)) { return false; }
			for (auto &ent : engine.entL) {
				if (ent->x == x && ent->y == y) {
					std::cout << "The " << ent->name << " avoids your attack!" << std::endl;
					return false;
				}
			}
			this->x = x; this->y = y;
			return true;
		}

We don't actually have any in-game mechanics to handle combat, so any attack will be met with a console message to inform us that "The monster avoids your attack!".

Next we want to create the `Ent::update()`, which will simply be a condition check to ensure the entity is within the player FOV, and then a debug message will be shown in the console as a means of confirming the entity is updating when in view:

	entity.cpp
		void Ent::update() { 
			if (engine.dungeon->isInFov(this->x, this->y)) { 
				std::cout << "The " << name << " grunts." << std::endl; 
			}
		}

## An improved engine

With all the hot new garbage we've implemented in our map and entity classes, it's about time the engine got a much deserved overhaul.

### engine.hpp

We're going to have the gameState be enumerated by the engine so that we can track if the game is at the `START` or `IDLE` or on a new `TURN` and so on.
Update the Engine class with a public for `enum gameState { START, IDLE, TURN, WIN, LOSE } gameState;`.

### engine.cpp

The changes to our engine implementation are numerous, but mostly minor. So I'm going to power through them top to bottom with the expectation that anyone reading will use the provided engine.cpp source file as a reference.

+ initialize the game state in the engine constructor `gameState(START),`
+ update the player creation to include a `"player",` string to fill its name field.
+ amend the `Engine::update()` implementation so that it begins with a gameState check, and if the `gameState == START`, compute the dungeon fov for the first time.
+ ensure that the gameState is set to `IDLE` with each call to update the engine.
+ update the player movement mechanism by introducing two new integers to represent input values on the x and y axis
+ have these new integers condition tested after the input switch so that if either have changed, the `gameState` is set to `TURN` and we test if `player->moveOrAttack` returns true against the player x + input x and player y + input y, if the test returns true call a `dungeon->computeFov();` to update the map.
+ and lastly on the `Engine::update()`, if `gameState == TURN` run an iterator to update each entity that is not the player using `if (ent != entL[0])` as the condition test, because we know entity 0 is the player.

## The creatures that go grunt in the night

Did you remember to update your **main.hpp** with the new includes? No? Then go do it now.

	main.hpp
		#pragma once
		
		#include <iostream>
		#include <vector>
		#include <memory>
		#include <random>
		#include <functional>
		#include <string>
		#include "libtcod.hpp"
		#include "entity.hpp"
		#include "map.hpp"
		#include "engine.hpp"

With that, part-05 is done, save your work, build your solution, compile and play.
As always, let me know if I've screwed anything up, and suggestions/critiques are also always welcome.
