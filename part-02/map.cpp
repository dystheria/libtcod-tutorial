#include "main.hpp"

Map::Map(int w, int h) : w(w), h(h) {
	tiles = new Tile[w*h];
//Create two "pillars", as we currently have no actual dungeon
	setWall(30, 22); setWall(50, 22);
}

Map::~Map() { delete [] tiles; }

bool Map::isWall(int x, int y) const { return !tiles[x + y * w].canWalk; }
void Map::setWall(int x, int y) { tiles[x + y * w].canWalk = false; }

void Map::render() const {
	static const TCODColor dWall(0, 0, 30);
	static const TCODColor dFloor(50, 50, 80);

	for (int x = 0; x < w; x++) {
		for (int y = 0; y < h; y++) {
			TCODConsole::root->setCharBackground(x, y, isWall(x, y) ? dWall : dFloor);
		}
	}
}