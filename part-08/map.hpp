struct Tile {
	bool explored; Tile() : explored(false) {}
};

class Map {
public:
	int w, h;

	Map(int w, int h); ~Map();
	
	bool canWalk(int x, int y) const,
		isWall(int x, int y) const,
		isInFov(int x, int y) const,
		isExplored(int x, int y) const;
	
	void computeFov(),
		addMonster(int x, int y);

	void render() const;

protected:
	Tile *tiles;
	
	std::shared_ptr<TCODMap> map;
	
	friend class bspLst;

	void dig(int x1, int y1, int x2, int y2),
		cRoom(bool first, int x1, int y1, int x2, int y2),
		addItem(int x, int y);
};