#include "main.hpp"

Engine::Engine(int sW, int sH) : gameState(START), fovRad(10),sW(sW),sH(sH) {
	TCODConsole::initRoot(sW, sH, "ALT|A Libtcod Tutorial v0.9", false);
	player = std::make_shared<Ent>(1, 1, '@', "player", TCODColor::white);
	player->mortal = std::make_shared<pcMortal>(30, 2, "your lifeless corpse");
	player->combat = std::make_shared<Combat>(5);
	player->ai = std::make_shared<PlayerAi>();
	player->container = std::make_shared<Container>(10);
	entL.push_back(player);

	dungeon = std::make_unique<Map>(80, 43);
	gui = new Gui();
	gui->message(TCODColor::red, "Welcome, stranger!\nI do hope you're ready to die.");
}

Engine::~Engine() { entL.clear(); delete gui; }

std::shared_ptr<Ent> Engine::getClosestMob(int x, int y, float range) const {
	std::shared_ptr<Ent> closest = NULL; float bestDist(1E6f);
	for (auto &ent : entL) {
		if (ent != player && ent->mortal) {
			if (!ent->mortal->isDead()) {
				float dist(ent->getDist(x, y));
				if (dist < bestDist && (dist <= range || range == 0.0f)) { bestDist = dist; closest = ent; }
	} }	}
	return closest;
}

std::shared_ptr<Ent> Engine::getMob(int x, int y) const {
	for (auto &ent : entL) {
		if (ent->x==x && ent->y==y) { if (ent->mortal) { if (!ent->mortal->isDead()) { return ent; } } }
	}
	return NULL;
}

bool Engine::pickTile(int *x, int *y, float maxRange) {
	while (!TCODConsole::isWindowClosed()) {
		render();
		for (int cx(0); cx < dungeon->w; cx++) {
			for (int cy(0); cy < dungeon->h; cy++) {
				if (dungeon->isInFov(cx, cy) && (maxRange == 0 || player->getDist(cx, cy) <= maxRange)) {
					TCODColor col = TCODConsole::root->getCharBackground(cx, cy); col = col * 1.2f;
					TCODConsole::root->setCharBackground(cx, cy, col);
		} }	}
		TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS|TCOD_EVENT_MOUSE, &lastKey, &mouse);
		if (dungeon->isInFov(mouse.cx, mouse.cy) && (maxRange == 0 || player->getDist(mouse.cx, mouse.cy) <= maxRange)) {
			TCODConsole::root->setCharBackground(mouse.cx, mouse.cy, TCODColor::black);
			if (mouse.lbutton_pressed) { *x=mouse.cx; *y=mouse.cy; return true; } 
		}
		if (mouse.rbutton_pressed) { return false; }
		TCODConsole::flush();
	}
	return false;
}

void Engine::update() {
	if (gameState == START) dungeon->computeFov();
	gameState = IDLE;
	TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS|TCOD_EVENT_MOUSE, &lastKey, &mouse);
	player->update(player);
	if (gameState == TURN) {
		for (auto &ent : entL) { if (ent->name != "player" && ent->mortal) { 
			if ( ! ent->mortal->isDead() ) { ent->update(ent); }
} } } }

void Engine::render() { 
	TCODConsole::root->clear(); dungeon->render(); 
	for (auto &ent : entL)  { if (dungeon->isInFov(ent->x, ent->y)) { ent->render(); } } 
	player->render(); gui->render();
}