#pragma once
#include <iostream>
#include <vector>
#include <memory>
#include <random>
#include <functional>
#include <string>
#include <utility>
#include <cstdarg>
#include "libtcod.hpp"
class Ent;
#include "mortal.hpp"
#include "combat.hpp"
#include "ai.hpp"
#include "loot.hpp"
#include "container.hpp"
#include "entity.hpp"
#include "map.hpp"
#include "gui.hpp"
#include "engine.hpp"