class Loot {
public:
	bool collect(std::shared_ptr<Ent> owner, std::shared_ptr<Ent> wearer);
	virtual bool use(std::shared_ptr<Ent> owner, std::shared_ptr<Ent> wearer);
};

class Healer : public Loot {
public:
	float amt; // amount of hp restored

	Healer(float amt);
	bool use(std::shared_ptr<Ent> owner, std::shared_ptr<Ent> wearer);
};