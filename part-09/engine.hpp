class Engine {
public:
	enum gameState { START, IDLE, TURN, WIN, LOSE } gameState;

	int fovRad, sW, sH;
	TCOD_mouse_t mouse;
	TCOD_key_t lastKey;

	std::vector<std::shared_ptr<Ent>> entL;
	std::shared_ptr<Ent> player;
	std::unique_ptr<Map> dungeon;

	std::shared_ptr<Ent> getClosestMob(int x, int y, float range) const;
	std::shared_ptr<Ent> getMob(int x, int y) const;

	Gui * gui;

	Engine(int sW, int sH); ~Engine();

	bool pickTile(int *x, int *y, float maxRange = 0.0f);

	void update(); void render();

private:
	bool computeFov;
};

extern Engine engine;