**Part 0 - getting set up.**

This tutorial will be a little unorthodox as I'm also learning C++ as I go, so it's a learning experience for everyone involved!
I'm going to be using [Microsoft Visual Studio 2017](https://imagine.microsoft.com/en-us/Catalog/Product/530), and [Libtcod 1.7.0](https://bitbucket.org/libtcod/libtcod/downloads/).

This entire tutorial will be based on the slightly outdated but still incredibly solid "roguelike from scratch" tutorial that you can find on [Roguebasin](http://www.roguebasin.com/index.php?title=Complete_roguelike_tutorial_using_C%2B%2B_and_libtcod_-_part_1:_setting_up#Why_libtcod_.3F)

Start by downloading and installing MS Visual Studio Community edition (it's totally free), and downloaded libtcod-1.7.0-x86_64-msvc.zip from the above link.
Once MS Visual Studio is installed, create your first project, select "Visual C++" and then "Windows Console Application".

**Getting MS Visual Studio to link to libtcod**

1. Make sure that you extract libtcod 1.7.0 somewhere easily accessible for linking to the MSVS project. I used D:\dev\dependencies\libtcod-1.7.0-x86_64-msvc
2. Once you've created your project, you should go to your libtcod directory and copy the following files from libtcod in to your project repository:
*libtcod.dll, libtcod.lib, libtcodgui.dll, libtcodgui.lib, SDL2.dll, terminal.png*
3. In your Visual Studio project, ensure that you've set the environment to "debug" and "x64" [like so](https://imgur.com/zUNmz2h)
4. Now you have to configure the additional includes and libraries for the x64 environment in Visual Studio, within the solution explorer window right click on your project and go to [properties](https://imgur.com/XvhqeAo)
5. Under *C++ > general* you want to include the location of your libtcod\include directory as an [additional include](https://imgur.com/G4amX1x)
6. Under *Linker > general* set the additional library directories to include the [root libtcod directory](https://imgur.com/r7PRMOI)
7. Under *Linker > input* set your additional dependencies to [include libtcod.lib and libtcodgui.lib](https://imgur.com/EuwSqTU)
8. And lastly, under *Linker > System* set your [subsystem to console](https://imgur.com/UnBi75l)

Now, a little disclaimer: I am completely unsure as to whether this tutorial has to be followed as a console application, so if anyone knows better than me, feel free to tell me off.

Anyway that should be you all set up and good to go with working with libtcod in your project!