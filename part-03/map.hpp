struct Tile {
	bool canWalk; Tile() : canWalk(false) {}
};

class Map {
public:
	int w, h;

	Map(int w, int h); ~Map();

	bool isWall(int x, int y) const;
	void render() const;

protected:
	Tile *tiles;
	friend class bspLst;
	void dig(int x1, int y1, int x2, int y2);
	void cRoom(bool first, int x1, int y1, int x2, int y2);
};