#pragma once
//this, in theory, prevents the header files from accidentally being called and compiled multiple times
//in essence keeping the filesize of our roguelike small

#include "libtcod.hpp"
//as libtcod is handling all of our more complex functions, this is all we need right now.