class Loot {
public:
	bool collect(std::shared_ptr<Ent> owner, std::shared_ptr<Ent> bearer);
	virtual bool use(std::shared_ptr<Ent> owner, std::shared_ptr<Ent> bearer);
	void drop(std::shared_ptr<Ent> owner, std::shared_ptr<Ent> bearer);
};

class Healer : public Loot {
public:
	float amt; // amount of hp restored

	Healer(float amt);
	bool use(std::shared_ptr<Ent> owner, std::shared_ptr<Ent> bearer);
};

class eBolt : public Loot {
public:
	float range, dmg;
	eBolt(float range, float dmg);
	bool use(std::shared_ptr<Ent> owner, std::shared_ptr<Ent> bearer);
};

class fBall : public eBolt {
public:
	fBall(float range, float dmg);
	bool use(std::shared_ptr<Ent> owner, std::shared_ptr<Ent> bearer);
};

class Confuser : public Loot {
public:
	int nbTurns;
	float range;
	Confuser(int nbTurns, float range);
	bool use(std::shared_ptr<Ent> owner, std::shared_ptr<Ent> bearer);
};