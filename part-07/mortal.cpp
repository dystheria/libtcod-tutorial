#include "main.hpp"

Mortal::Mortal(float MaxHp, float def, const char * corpseName) :
	MaxHp(MaxHp), hp(MaxHp), def(def), corpseName(corpseName) {}

float Mortal::takeDamage(std::shared_ptr<Ent> owner, float dmg) {
	dmg -= def;
	if (dmg > 0) { hp -= dmg; if (hp <= 0) { die(owner); } } else { dmg = 0; }
	return dmg;
}

void Mortal::die(std::shared_ptr<Ent> owner) {
	owner->ch = '%';
	owner->col = TCODColor::darkRed;
	owner->name = corpseName;
	owner->blocks = false;
}

pcMortal::pcMortal(float MaxHp, float def, const char * corpseName) :
	Mortal(MaxHp,def,corpseName) {}

npcMortal::npcMortal(float MaxHp, float def, const char * corpseName) :
	Mortal(MaxHp, def, corpseName) {}

void pcMortal::die(std::shared_ptr<Ent> owner) {
	engine.gui->message(TCODColor::red, "You have died!");
	Mortal::die(owner);
	engine.gameState = Engine::LOSE;
}

void npcMortal::die(std::shared_ptr<Ent> owner) {
	engine.gui->message(TCODColor::lightGrey, "The %s has perished.", owner->name);
	Mortal::die(owner);
}