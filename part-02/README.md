# Part-02 : Maps, Entities and the game engine.

So things appear to blow up a bit from only 2 files, to 8 files.
The intent here is to structure the program so that all the objects we're working with are easy for us to find and reference at any given point.

Start by creating the 3 header files and 3 source files we'll need:
*engine.hpp, entity.hpp, map.hpp,* **and** *engine.cpp, entity.cpp, map.cpp*.

----------

### Basic Entities

The least complex of these (at this stage) will be the entity header and implementation.

	entity.hpp:
		class Ent {
		public:
			int x, y, ch; TCODColor col;

			Ent(int x, int y, int ch, const TCODColor &col);
			void render() const;
		};

At this point we want each entity to have an x position, a y position, and a character code to represent them. We can get away with declaring the character code as an integer because C++ will convert the any character we provide in to it's respective ASCII value.

It's beyond the scope of this tutorial to explain, but using an int rather than char or unsigned char allows us a lot more room than just 256 characters.
We also want a TCODColor value so that elements of the game can be different colors for ease.

We need to declare a constructor for Entities, with the expectation that we'll have our 4 variables passed when declaring any new Ent, and then we want a render() const function for when we need to render any entities in the future.

And now the source file.

	entity.cpp:
		#include "main.hpp"
		Ent::Ent(int x, int y, int ch, const TCODColor &col) : x(x),y(y),ch(ch),col(col) {}
		
		void Ent::render() const {
			TCODConsole::root->setChar(x, y, ch);
			TCODConsole::root->setCharForeground(x, y, col);
		}

We only need to include our main header file, our constructor is... uninspired, but it's straight forward and works.

The render() function is also straight forward and tells the root TCODConsole where to place the character and the foreground col value for any Entity.

----------

### Basic Maps, Tile properties, and a simple dungeon.

	map.hpp
		struct Tile {
			bool canWalk; Tile() : canWalk(true) {}
		};

		class Map {
		public:
			int w, h;

			Map(int w, int h); ~Map();

			bool isWall(int x, int y) const;
			void render() const;

		protected:
			Tile * tiles;
			void setWall(int x, int y);
		};

We declare a struct for tiles which handles a boolean variable *canWalk* and we set the default for this as true so we can walk on everything by default.

We declare a class for our Map that contains the basic constraints for any map: the width(**w**), and the height (**h**).
So that we can check which tiles are walls we create an *isWall* boolean that takes the x and y integers of any tile. And of course because map tiles are a visible game element, we need to render the map tiles.

So that nothing else within the game can accidentally manipulate map tiles outside of the map constructor, we'll protect our tile variables, and we'll protect the setWall function that will control the placement of wall tiles.

	map.cpp
		#include "main.hpp"
		
		Map::Map(int w, int h) : w(w), h(h) {
			tiles = new Tile[w*h];
			setWall(30, 22); setWall(50, 22);
		}
		
		Map::~Map() { delete [] tiles; }
		
		bool Map::isWall(int x, int y) const { return !tiles[x + y * w].canWalk; }
		void Map::setWall(int x, int y) { tiles[x + y * w].canWalk = false; }
		
		void Map::render() const {
			static const TCODColor dWall(0, 0, 30);
			static const TCODColor dFloor(50, 50, 80);
		
			for (int x = 0; x < w; x++) {
				for (int y = 0; y < h; y++) {
					TCODConsole::root->setCharBackground(x, y, isWall(x, y) ? dWall : dFloor);
				}
			}
		}

From top to bottom:
We include our main header, we create a constructor for Map which takes the width and height of the map, and creates an array of tiles for every co-ordinate on the map. We're not going to be creating any fancy dungeons right now but we need to test our walls (setWall and isWall) to ensure we haven't made any mistakes, so we'll declare two single point, static pillars.

Our map deconstructor simply needs to delete all the tiles in our map array.

We define the bool isWall and void setWall functions so that they can be used in checking/creating map walls.

Now to render the dungeon we want some static color constants for the walls and floors.

And lastly, we run a for loop spanning the height of the map, within a for loop spanning the width of the map, to set the color of every tile based on whether it "isWall" or not. True and the tile is colored as a dark wall, false and it's a dark floor.

----------

### The Meaty Bits

	engine.hpp
		class Engine {
		public:
			TCODList<Ent *> entL;
			Ent *player; Map *dungeon;
		
			Engine(); ~Engine();
		
			void update(); void render();
		};
		
		extern Engine engine;

The first public declaration of the Engine is a list of all entities on the Map, *entL*.
We need a player entity, and a dungeon map, an engine constructor and deconstructor, and an update and render function.

Declaring an identifier as an extern tells the compiler that your identifier is a global identifier that will be declared in another source file.

	engine.cpp
		#include "main.hpp"
		
		Engine::Engine() {
			TCODConsole::initRoot(80, 50, "ALT|A Libtcod Tutorial v0.2", false);
			player = new Ent(40, 25, '@', TCODColor::white); entL.push(player);
			entL.push(new Ent(60, 13, '@', TCODColor::yellow));
			dungeon = new Map(80, 50);
		}
		
		Engine::~Engine() { entL.clearAndDelete(); delete dungeon; }
		
		void Engine::update() {
			TCOD_key_t key;
			TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS, &key, NULL);
			switch (key.vk) {
				case TCODK_UP: if (!dungeon->isWall(player->x, player->y - 1)) { player->y--; }; break;
				case TCODK_DOWN: if (!dungeon->isWall(player->x, player->y + 1)) { player->y++; }; break;
				case TCODK_LEFT: if (!dungeon->isWall(player->x - 1, player->y)) { player->x--; }; break;
				case TCODK_RIGHT: if (!dungeon->isWall(player->x + 1, player->y)) { player->x++; }; break;
				default:break;
			}
		}
		
		void Engine::render() { 
			TCODConsole::root->clear(); dungeon->render(); 
			for (Ent **itr = entL.begin(); itr != entL.end(); itr++) { (*itr)->render(); }
		}

So now we face a more substantial piece of source code, but it's all just declarations of our functions.

The Engine() constructor initializes our root TCODConsole, defines our player entity and then pushes it to the entities list, entL. We push a second new entity as a stand in NPC that won't do anything or even be material, simply visible in the game world.
And we declare the dungeon map, 80 tiles wide, 50 tiles high.

Engine deconstructor: clear and delete the entities list, delete the dungeon map.

the update function will now handle our movement, it's very similar to the first iteration found in main.cpp in part-01, but this time round we're going to have each keystroke make a check to see if we can even move in to the proposed tile.
To deconstruct these instructions, we're asking the game engine, "when you see this key be pressed, call the dungeon function 'isWall' and compare it against the block that the character is trying to move in to, if that block is NOT a wall, the player position can move."

If you're new to C++ you might be wondering what -> is and why we're using it. -> is a pointer arrow, it isn't an address pointer, but instead is used to point to an identifier of a given object. So dungeon, being a Map object, should have an "isWall" function, we can point to that function by using dungeon->iswWall. player->x and player->y literally just pass through the x and y values of the player object.

Lastly, we have the enginer renderer, which we give a command to clear the root console, then to render the dungeon map, and lastly to loop through every entry on the entities list and render it.
We complete this loop with an iterator, an iterator is used to "iterate" through items within an array, list, collection or vector, and apply an action or check to it. In this case, we're rendering each actor before increasing the iterator to the next object, until we come to the end of the entities list.

----------

### Updating the main

	main.hpp
		#pragma once
		
		#include "libtcod.hpp"
		#include "entity.hpp"
		#include "map.hpp"
		#include "engine.hpp"

The main header is pretty self explanatory, we call each header-file in order of dependency to ensure the program compiles correctly.
libtcod.hpp is our main dependency as all our other source files make calls to functions within libtcod.
We make calls to entity.hpp in both map.hpp and engine.hpp, so it is the next dependency we need, with engine.hpp coming last because none of the functions within engine.hpp or engine.cpp are called by the previous dependents.

It's important to always ensure your include order is listed by order of dependency to prevent the compiler throwing up errors about undeclared symbols or externals. Compilers are not intelligent, when they hit an error as a result of improperly ordered dependencies they don't realize after the fact "oh, the class you wanted to reference is actually in this dependency you included AFTER including the header we're currently working in."

	main.cpp
		#include "main.hpp"
		
		Engine engine;
		
		int main() {
			while (!TCODConsole::isWindowClosed()) {
				engine.update(); engine.render();
				TCODConsole::flush();
			}
		}

This file should be self explanatory, but as I've made a habit of walking through the entire source lets carry on like we started.

We include our main header, and then we declare the global identifier "engine" that we informed the engine.hpp header file was external.
**Do not forget to do this**, without the external engine global, we don't have an engine called to run the game, and the compiler should throw up an error anyway telling you that you have an unresolved external.

Lastly, we run our main loop, so as with part-01, while the TCODConsole window is not closed, we repeatedly call the engine.update() function, then the engine.render() function, and lastly flush the console to display any updates and rendering.

That's us, we're done part 2! Assuming I haven't made any horrible mistakes in this tutorial you should be good to either compile and test, or hit the local windows debugger and play around a little.