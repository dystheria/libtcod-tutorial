class Ent {
public:
	int x, y, ch; TCODColor col;
	const std::string name;

	Ent(int x, int y, int ch, const std::string name, const TCODColor &col);
	
	bool moveOrAttack(int x, int y);
	
	void update();

	void render() const;
};