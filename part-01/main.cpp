//ONE HEADER TO FIND THEM, AND IN THE DARKNESS BIND THEM
//the roguebasin tutorial does a lot of unnecessary header file management.
//lest use a single header from the start to save us future problems.
#include "main.hpp"

//A simple main routine
int main() {
	//declare the x and y player position variables
	int playerx = 40, playery = 25;
	//initialize a console of 80 wide by 50 high, give it a fancy title, set fullscreen to "false"
	TCODConsole::initRoot(80, 50, "ALT|A libtcod tutorial v0.1", false);
	//While the window isn't closed, do the following ...
	while (!TCODConsole::isWindowClosed()) {
		//set "key" to TCOD_ket_t
		TCOD_key_t key;
		//wait for any key press
		TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS, &key, NULL);
		//handle the x and y player position variables with a switch
		switch (key.vk) {
			case TCODK_UP: playery--; break; //arrow key up, subtract 1 from playery
			case TCODK_DOWN: playery++; break; //arrow key down, add 1 to playery
			case TCODK_LEFT: playerx--; break; //etcetera for x
			case TCODK_RIGHT: playerx++; break; //and again...
			//break in the case of any other press
			default:break;
		}
		//clear the console we initialized
		TCODConsole::root->clear();
		//slap the '@' symbol for our player on it at "playerx","playery" position
		TCODConsole::root->putChar(playerx, playery, '@');
		//flush the console, effectively drawing it out visibly for us, the player.
		TCODConsole::flush();
	}
}