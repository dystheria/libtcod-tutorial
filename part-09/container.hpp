class Container {
public:
	int size; // maximum container size.
	std::vector<std::shared_ptr<Ent>> inventory;

	Container(int size); ~Container();
	
	bool add(std::shared_ptr<Ent> ent);
	
	void del(std::shared_ptr<Ent> ent);
};