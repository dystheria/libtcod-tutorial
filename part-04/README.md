# Part 4, field of view

The original article for section four can be found [here](http://www.roguebasin.com/index.php?title=Complete_roguelike_tutorial_using_C%2B%2B_and_libtcod_-_part_4:_field_of_view)

Don't forget you can hop over to [RogueLikeDev on reddit](http://https://www.reddit.com/r/roguelikedev/) for advice, guidance and general discussion about roguelike development.

### Preface and Warning

**FIRST**, I owe massive thanks to [u/DontEatSoapDudley](http://www.reddit.com/user/donteatsoapdudley) and [u/thebracket](http://www.reddit.com/user/thebracket) for their guidance and wisdom in moving away from some of the more archaic practices that are in the base tutorial on roguebasin. 

**SECOND**, My apologies to anyone following along piece by piece, during parts 4 and 5 I began discovering just how unimportant white-space is in C++ and how flexible the syntax of the language is, as a result there is a rather sharp change in my coding style as I developed a style that I personally am far more comfortable with.

I have also been made aware of several standard library functions and have made a concious move towards using the `<memory>` and `<vector>` includes to replace the functionality of standard raw pointers and the `TCODList` function.

Additionally, this tutorial will become less of a hand-holding exercise from here onwards, with each section only covering what improvements are being made to each source file.

## Refactoring our use of pointers and implementing some standard libraries.

### main.hpp

At the start of our include list we need to bring in some standard library functions, in order these are `<vector>` and `<memory>`.
`<vector>` will become our active replacement for the TCODList object, and `<memory>` will allow us access to smart pointers.

### engine.hpp

Our entity list will become a vector of shared pointers to Entities rather than a TCODList of raw pointers `std::vector<std::shared_ptr<Ent>> entL;`, the player definition stays the same, but the dungeon definition now becomes a unique pointer to a Map object, `std::unique_ptr<Map> dungeon;`.

### engine.cpp

Having changed our raw pointers to smart pointers, we need to change the implementation, deleting our standard player initialization and replacing it with `entL.emplace_back(std::make_shared<Ent>(1,1,'@', TCODColor::white)); player = entL[0].get();`.
The dungeon initilization now also needs to be improved to become `dungon = std::make_unique<Map>(80,50);`.

Our iterator in the `Engine::render()` function is also a rather type-heavy method of calling iterators, C++ has the ability to carry out "Ranged For" loops, we can replace our iterator with the following instead. `for (auto &ent : entL) {ent->render();}`, the use of auto is also a helpful tool that allows the compiler to decide the object type of a declaration, this is a good step in avoiding human error as the compiler will only allocate an object type that can result in a legal execution of the command, so in this instance the compiler recognizes that `entL` only contains object pointers of the type `Ent`, so this type definition for `&ent` cannot possibly be anything other than `Ent` class objects.

This move to vectors and smart pointers means we also need to revise our deconstructor. `Engine::~Engine() {entL.clear();}` will do the job.

### map.cpp

Our **map.cpp** source file still refers to the entities list as though it were both a TCODList object and containing a list of raw pointer objects. We need to update the cRoom function so that it now has a 1 in 4 chance of executing `engine.entL.emplace_back(std::make_shared<Ent>(cx,cy,'@',TCODColor::yellow));`.

If you're not in the habit of frequently saving your work, now would be a good time to do a save and compile just to make sure everything still works with the new code changes, you can of course just take a copy of my completed source to compare against your own and assist with debugging/troubleshooting.

# THE ACTUAL PART 4

## Improving our map class for use with TCODMap

In order to make the game environment a little more interactive it's necessary to provide each tile of the gameworld with more attributes, we've covered whether a tile is a wall or a floor and whether it is possible for the player to walk on the tile as a result. We now need to consider if the player character has *seen* the tile in question (have they already explored the tile), and whether it is in their active field of vision or not (can they see what is happening on that tile at the present moment in time).

### map.hpp

Within the **Map Class** we're going to create a shared pointer to a TCODMap object for our map, and create two booleans for `isInFov` and `isExplored`, these booleans will need to pass the x and y coordinate of the tile in question, be sure to declare them as const to prevent accidental alteration of the bool state.

Once we've done that, we need to update our Tile struct and replace `bool canWalk` with `bool explored`, much like with canWalk, the default explored state of any tile should be `explored(false)`.

Lastly, we will need to create a void that will handle computing the FOV on the map each turn. `void computeFov();`

### map.cpp

We need to update our constructor to allocate the TCODMap object with `map = std::make_shared<TCODMap>(w,h);`, the use of a shared pointer means we do not need to delete the object in our deconstructor, the pointer is a smart pointer and will count the pointer occurences in the background until the pointer occurence reaches zero, at which point it will consider the pointer out of scope, and automatically delete it. No memory leaks and no need to worry about the order of manual deletion.
We also need to update **isWall** to return `!map->isWalkable(x,y);` in place of the `!tiles[x+y*w].canWalk;`. **isWalkable** is an inherent function of the TCODMap object and will provide us with the same functionality.
To create our **isExplored** bool we will simply have it `return[x+y*w].explored;`

Next comes the **isInFov** bool, the logic of this should be that if the x and y relate to a tile that is in our field of view, the tile becomes `explored` and the bool returns `true`, if the tile is not in our field of view, `false` is returned.
`if(map->isInFov(x,y)){tiles[x+y*w].explored=true; return true;}`

Moving on to our void funcitons, `void Map::dig` no longer uses `canWalk` and will instead make use of the TCODMap **setProperties** function, so replace the `tiles[tx+ty*w].canWalk=true;` with `map->setProperties(tx,ty,true,true);`.
create the `Map::computeFov()` to computer the fov based on the player x and y positions, with a radius value that we will allocate to a field on the engine class later on, named fovRad, the source should look like this.

	map.cpp
		void Map::computeFov() {
			map->computeFov(engine.player->x, engine.player->y, engine.fovRad);
		}

Each tile now has three attributes that impact visibility in the game world, is it explored? is it in Fov? is it a floor or a wall tile? To make these states apparent to the player, we should use different colors for each state, with black being the default for any tile that is unexplored, regardless of floor/wall status.
I opted to streamline the `void Map::render() const` function while making these improvements, because C++ doesn't care about excess white-space and indentation you can declare all four tile color states in a single static const declaration.
We'll also update the Map drawing in the `Map::render()` tile cycle so that it accounts for explored and unexplored tiles.

	map.cpp
		void Map::render() const {
			static const TCODColor dWall(0, 0, 30), dFloor(30, 30, 50), 
				lWall(80, 80, 110), lFloor(100, 100, 100);
		
			for (int x = 0; x < w; x++) {
				for (int y = 0; y < h; y++) {
					if (isInFov(x, y)) {
						TCODConsole::root->setCharBackground(x, y, isWall(x, y) ? lWall : lFloor);
					} else if (isExplored(x,y)) {
						TCODConsole::root->setCharBackground(x, y, isWall(x, y) ? dWall : dFloor);
		} } } }

## Engine improvements.

### engine.hpp

All we need to do here is make sure we have a public declaration for our fov radius, we'll want it to be an integer as it'll handle whole tile numbers and nothing else, `int fovRad;` and we want a private boolean to track the status of whether the fov has been computed or not, `bool computeFov;`

### engine.cpp

Update the engine constructor with an initialization list for the fov radius and to set the fov compute state to true so that our very first frame is properly rendered. `Engine::Engine() : fovRad(10), computeFov(true) {`. Now this game isn't real-time, it's turn based, so we only need to update the game-engine after the player moves, so in each switch case we want to call a `computerFov=true;` after each movement, but before the case switch `break;`.
We haven't actually implemented anything to call the `dungeon->computerFov()` function at this point, so test for the condition of `computeFov` and if it returns true, call the `dungeon->computeFov();` and reset the `computeFov` bool back to false.
This should be the very last execution in the `Engine::update()` implementation

We also only want to render entities that are actively in our FOV, so add the condition `if(dungeon->isInFov(ent->x,ent->y)) {ent->render();}` to our ranged for loop

## save and test.

That's all we should need for part 4, save, rebuild, and give it a whirl.

If you notice any issues or errors be sure to let me know! And have fun wandering your now explorable dungeons.