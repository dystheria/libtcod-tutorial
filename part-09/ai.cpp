#include "main.hpp" 


// player AI starts here

void PlayerAi::update(std::shared_ptr<Ent> owner) {
	if (owner->mortal && owner->mortal->isDead()) { return; }
	int ix(0), iy(0);
	switch (engine.lastKey.vk) {
	case TCODK_UP: iy = -1; break;
	case TCODK_DOWN: iy = +1; break;
	case TCODK_LEFT: ix = -1; break;
	case TCODK_RIGHT: ix = +1; break;
	case TCODK_CHAR: handleActionKey(owner, engine.lastKey.c); break;
	default:break;
	}
	if (ix != 0 || iy != 0) {
		engine.gameState = Engine::TURN;
		if (moveOrAttack(owner, owner->x+ix, owner->y+iy)) { engine.dungeon->computeFov(); }
}}

void PlayerAi::handleActionKey(std::shared_ptr<Ent> owner, int ascii) {
	switch (ascii) {
	case 'c' : // collect item
		{
			bool found = false;
			for (auto &ent : engine.entL) {
				if (ent->loot && ent->x == owner->x && ent->y == owner->y) {
					auto lootname(ent->name);
				if ( ent->loot->collect(ent,owner)) { 
					found = true; engine.gui->message(TCODColor::lightGrey, "You collect the %s.", lootname);
					break;
				} else if (! found) {
					found = true; engine.gui->message(TCODColor::red, "Your inventory is full.");
			} } } 
			if (!found) { engine.gui->message(TCODColor::lightGrey, "There's nothing here."); }
		engine.gameState = Engine::TURN;
		} break;
	case 'i' : // access inventory
		{
			std::shared_ptr<Ent> item=chooseFromInv(owner);
			if (item) {
				item->loot->use(item, owner);
				engine.gameState = Engine::TURN;
			}
		}
		break;
} }

bool PlayerAi::moveOrAttack(std::shared_ptr<Ent> owner, int tx, int ty) {
	if (engine.dungeon->isWall(tx, ty)) return false;
	for (auto &ent : engine.entL) { 
		if (ent->mortal && ! ent->mortal->isDead() && ent->x == tx && ent->y == ty) {
			owner->combat->attack(owner, ent); return false;
	} }
	for (auto &ent : engine.entL) {
		bool corpseOrLoot((ent->mortal && ent->mortal->isDead()) || ent->loot);
		if (corpseOrLoot && ent->x == tx && ent->y == ty ) {
			engine.gui->message(TCODColor::white, "There is a %s here.", ent->name);
	} }
	owner->x = tx; owner->y = ty; return true;
}

// Mob AI starts here

static const int TRACK_TURNS(3);

MobAi::MobAi() : moveCount(0) {}

void MobAi::update(std::shared_ptr<Ent> owner) {
	if (owner->mortal && owner->mortal->isDead()) { return; }
	if (engine.dungeon->isInFov(owner->x, owner->y)) { moveCount = TRACK_TURNS; }
	else { moveCount--; }
	if (moveCount > 0) { moveOrAttack(owner, engine.player->x, engine.player->y); }
}

void MobAi::moveOrAttack(std::shared_ptr<Ent> owner, int tx, int ty) {
	int dx(tx - owner->x), dy(ty - owner->y), sdx(dx > 0 ? 1:-1), sdy(dy > 0 ? 1:-1), distance((int)sqrt(dx*dx + dy * dy));
	if (distance >= 2) {
		dx = (int)(round(dx / distance)); dy = (int)(round(dy / distance));
		if (engine.dungeon->canWalk(owner->x + dx, owner->y + dy)) { owner->x += dx; owner->y += dy; }
		else if (engine.dungeon->canWalk(owner->x + sdx, owner->y)) { owner->x += sdx; }
		else if (engine.dungeon->canWalk(owner->x, owner->y + sdy)) { owner->y += sdy; }
	}
	else if (owner->combat) { owner->combat->attack(owner, engine.player); }
}

ConfusedMobAi::ConfusedMobAi(int nbTurns, std::shared_ptr<Ai> oldAi) : nbTurns(nbTurns), oldAi(oldAi) {}

void ConfusedMobAi::update(std::shared_ptr<Ent> owner) {
	TCODRandom *rng = TCODRandom::getInstance();
	int dx(rng->getInt(-1, 1)), dy(rng->getInt(-1, 1));
	if (dx != 0 || dy != 0) { 
		int destx(owner->x+dx),desty(owner->y+dy);
		if (engine.dungeon->canWalk(destx, desty)) {
			owner->x=destx; owner->y=desty;
		} else {
			std::shared_ptr<Ent> targetMob;
			targetMob = engine.getMob(destx, desty);
			if (targetMob) { owner->combat->attack(owner, targetMob); }
		} }
	nbTurns--;
	if (nbTurns==0) {
		owner->ai = oldAi;
} }

std::shared_ptr<Ent> PlayerAi::chooseFromInv(std::shared_ptr<Ent> owner) {
	static const int invW(50), invH(28); static TCODConsole con(invW, invH);
	con.setDefaultForeground(TCODColor(200, 180, 50));
	con.printFrame(0, 0, invW, invH, true, TCOD_BKGND_DEFAULT, "Inventory");
	con.setDefaultForeground(TCODColor::white);
	
	int is('a'); int i(1);
	for (auto &item : owner->container->inventory) {
		con.print(2, i, "(%c) %s", is, item->name); i++; is++;
	}

	TCODConsole::blit(&con, 0, 0, invW, invH, TCODConsole::root, engine.sW / 2 - invW / 2, engine.sH / 2 - invH / 2);
	TCODConsole::flush();

	TCOD_key_t key;
	TCODSystem::waitForEvent(TCOD_EVENT_KEY_PRESS, &key, NULL, true);
	if ( key.vk == TCODK_CHAR) {
		int itemIndex(key.c - 'a');
		if (itemIndex >= 0 && itemIndex < owner->container->inventory.size()) {
			return owner->container->inventory.at(itemIndex);
		}
	}
	return NULL;
}