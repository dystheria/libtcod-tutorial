class Engine {
public:
	int fovRad;

	std::vector<std::shared_ptr<Ent>> entL;
	Ent * player; 
	std::unique_ptr<Map> dungeon;

	Engine(); ~Engine();

	void update(); void render();

private:
	bool computeFov;
};

extern Engine engine;