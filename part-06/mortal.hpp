class Mortal {
public:
	float MaxHp, hp, def;
	const char *corpseName;
	
	Mortal(float MaxHp, float def, const char *corpseName);
	inline bool isDead() { return hp <= 0; }
	float takeDamage(std::shared_ptr<Ent> owner, float dmg);
	virtual void die(std::shared_ptr<Ent>  owner);
};

class pcMortal : public Mortal {
public:
	pcMortal(float MaxHp, float def, const char *corpseName);
	void die(std::shared_ptr<Ent> owner);
};

class npcMortal : public Mortal {
public:
	npcMortal(float MaxHp, float def, const char *corpseName);
	void die(std::shared_ptr<Ent> owner);
};
