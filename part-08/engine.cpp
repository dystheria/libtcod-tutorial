#include "main.hpp"

Engine::Engine(int sW, int sH) : gameState(START), fovRad(10),sW(sW),sH(sH) {
	TCODConsole::initRoot(sW, sH, "ALT|A Libtcod Tutorial v0.8", false); //try to remember to update the version each time!
	player = std::make_shared<Ent>(1, 1, '@', "player", TCODColor::white);
	player->mortal = std::make_shared<pcMortal>(30, 2, "your lifeless corpse");
	player->combat = std::make_shared<Combat>(5);
	player->ai = std::make_shared<PlayerAi>();
	player->container = std::make_shared<Container>(10);
	entL.push_back(player);

	dungeon = std::make_unique<Map>(80, 43);
	gui = new Gui();
	gui->message(TCODColor::red, "Welcome, stranger!\nI do hope you're ready to die.");
}

Engine::~Engine() { entL.clear(); delete gui; }

void Engine::update() {
	if (gameState == START) dungeon->computeFov();
	gameState = IDLE;
	TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS|TCOD_EVENT_MOUSE, &lastKey, &mouse);
	player->update(player);
	if (gameState == TURN) {
		for (auto &ent : entL) { if (ent->name != "player" && ent->mortal) { 
			if ( ! ent->mortal->isDead() ) { ent->update(ent); }
} } } }

void Engine::render() { 
	TCODConsole::root->clear(); dungeon->render(); 
	for (auto &ent : entL)  { if (dungeon->isInFov(ent->x, ent->y)) { ent->render(); } } 
	player->render(); gui->render();
}