#include "main.hpp"

Engine::Engine() {
	TCODConsole::initRoot(80, 50, "ALT|A Libtcod Tutorial v0.2", false);
	player = new Ent(40, 25, '@', TCODColor::white); entL.push(player);
	entL.push(new Ent(60, 13, '@', TCODColor::yellow));
	dungeon = new Map(80, 50);
}

Engine::~Engine() { entL.clearAndDelete(); delete dungeon; }

void Engine::update() {
	TCOD_key_t key;
	TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS, &key, NULL);
	switch (key.vk) {
		case TCODK_UP: if (!dungeon->isWall(player->x, player->y - 1)) { player->y--; }; break;
		case TCODK_DOWN: if (!dungeon->isWall(player->x, player->y + 1)) { player->y++; }; break;
		case TCODK_LEFT: if (!dungeon->isWall(player->x - 1, player->y)) { player->x--; }; break;
		case TCODK_RIGHT: if (!dungeon->isWall(player->x + 1, player->y)) { player->x++; }; break;
		default:break;
	}
}

void Engine::render() { 
	TCODConsole::root->clear(); dungeon->render(); 
	for (Ent **itr = entL.begin(); itr != entL.end(); itr++) { (*itr)->render(); }
}

