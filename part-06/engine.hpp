class Engine {
public:
	enum gameState { START, IDLE, TURN, WIN, LOSE } gameState;

	int fovRad, sW, sH;
	TCOD_key_t lastKey;

	std::vector<std::shared_ptr<Ent>> entL;
	std::shared_ptr<Ent> player;
	std::unique_ptr<Map> dungeon;
	
	Engine(int sW, int sH); ~Engine();

	void update(); void render();

private:
	bool computeFov;
};

extern Engine engine;