class Ai {
public: 
	virtual void update(std::shared_ptr<Ent>  owner) = 0;
};

class PlayerAi : public Ai {
public:
	void update(std::shared_ptr<Ent> owner);

protected:
	void handleActionKey(std::shared_ptr<Ent> owner, int ascii);
	bool moveOrAttack(std::shared_ptr<Ent> owner, int tx, int ty);
	std::shared_ptr<Ent> chooseFromInv(std::shared_ptr<Ent> owner);
};

class MobAi : public Ai {
public:
	void update(std::shared_ptr<Ent> owner);

protected:
	int moveCount;
	void moveOrAttack(std::shared_ptr<Ent> owner, int tx, int ty);
};