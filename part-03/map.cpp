#include "main.hpp"

static const int rsMIN = 8, rsMAX = 12;

class bspLst : public ITCODBspCallback {
private:
	Map &dungeon; int rNum; int lastx, lasty;
public:
	bspLst(Map &dungeon) : dungeon(dungeon), rNum(0) {}
	bool visitNode(TCODBsp *node, void *userData) {
		if (node->isLeaf()) {
			int x, y, w, h;
			TCODRandom *rng = TCODRandom::getInstance();
			w = rng->getInt(rsMIN, node->w - 2);
			h = rng->getInt(rsMIN, node->h - 2);
			x = rng->getInt(node->x + 1, node->x + node->w - w - 1);
			y = rng->getInt(node->y + 1, node->y + node->h - h - 1);
			dungeon.cRoom(rNum == 0, x, y, x + w - 1, y + h - 1);
			if (rNum != 0) {
				dungeon.dig(lastx, lasty, x + w / 2, lasty);
				dungeon.dig(x + w / 2, lasty, x + w / 2, y + h / 2);
			}
			lastx = x + w / 2 ;	lasty = y + h / 2;	rNum++;
		}
		return true;
} };

Map::Map(int w, int h) : w(w), h(h) {
	tiles = new Tile[w*h];
	TCODBsp bsp(0, 0, w, h);
	bsp.splitRecursive(NULL, 8, rsMIN, rsMAX, 1.5f, 1.5f);
	bspLst listener(*this);
	bsp.traverseInvertedLevelOrder(&listener, NULL);
}
Map::~Map() { delete [] tiles; }

void Map::dig(int x1, int y1, int x2, int y2) {
	if (x2 < x1) { int tmp = x2; x2 = x1; x1 = tmp; }
	if (y2 < y1) { int tmp = y2; y2 = y1; y1 = tmp; }
	for (int tx = x1; tx <= x2; tx++) {
		for (int ty = y1; ty <= y2; ty++) {
			tiles[tx + ty * w].canWalk = true;
} }	}

void Map::cRoom(bool first, int x1, int y1, int x2, int y2) {
	dig(x1, y1, x2, y2);
	int cx((x1 + x2) / 2), cy((y1 + y2) / 2);
	if (first) { 
		engine.player->x = cx; engine.player->y = cy;
	} else {
		TCODRandom *rng = TCODRandom::getInstance();
		if (rng->getInt(0, 3) == 0) { engine.entL.push(new Ent(cx, cy, '@', TCODColor::yellow)); }
} }

bool Map::isWall(int x, int y) const { return !tiles[x + y * w].canWalk; }

// void Map::setWall(int x, int y) { tiles[x + y * w].canWalk = false; } // we don't need this anymore so we can delete it.

void Map::render() const {
	static const TCODColor dWall(0, 0, 30);
	static const TCODColor dFloor(50, 50, 80);

	for (int x = 0; x < w; x++) {
		for (int y = 0; y < h; y++) {
			TCODConsole::root->setCharBackground(x, y, isWall(x, y) ? dWall : dFloor);
} } }