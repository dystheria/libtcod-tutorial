#include "main.hpp"

Container::Container(int size) : size(size) {}
Container::~Container() { inventory.clear(); }

bool Container::add(std::shared_ptr<Ent> ent) {
	if (size > 0 && inventory.size() >= size) { return false; }
	inventory.push_back(ent); return true;
}

void Container::del(std::shared_ptr<Ent> ent) {
	auto it = find(inventory.begin(), inventory.end(), ent);
	inventory.erase(it);
}