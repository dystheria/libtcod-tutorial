# Part-06 Extremely basic combat and Mobs

The roguebasin tutorial for this section can be found [here](http://www.roguebasin.com/index.php?title=Complete_roguelike_tutorial_using_C%2B%2B_and_libtcod_-_part_6:_going_berserk!).

If you're not aware of it (and you really should be by now), check out [r/RogueLikeDev](https://www.reddit.com/r/roguelikedev/) for advice, guidance, tips and discussion.

This section of the tutorial will cover basic combat mechanics, very basic "artificial" intelligence, and entity mortality.
The first thing you're going to want to do is create three new headers and three new source files for **Ai**, **Combat**, and **Mortal** aspects of in-game entities.

Much like the previous article I will only dissect new code in this article, so use my source files for guidance if you get stuck.

## Headers and Mains

Before we do ANYTHING ELSE, we're going to need to update our main header file to ensure that we don't have constant error squiggles in the IDE.

We're going to be referencing the entity class in our new feature source files but we're also going to be referncing the new entity features in our entity class as well... so how are we meant to order them if technically the Entity class needs to come before, and after, the respective features.

Well we do it by directly informing the compiler that the Ent class exists, before the Entity feature headers. Like so:

	main.hpp	
		#include <iostream>
		#include <vector>
		#include <memory>
		#include <random>
		#include <functional>
		#include <string>
		#include "libtcod.hpp"
		class Ent;
		#include "mortal.hpp"
		#include "combat.hpp"
		#include "ai.hpp"
		#include "entity.hpp"
		#include "map.hpp"
		#include "engine.hpp"

Easy when you know how.

### main.cpp

All we need to do here is update the `Engine engine();` constructor to take the width and height of the game engine as arguments, so `Engine engine(80,50);`, we'll be updating the functionality of the Engine constructor next to facilitate this.

## Improved Engine

These fancy new Entity features will require equally fancy implementations, but will also allow us to separate out how things update within the engine, as well as where and how things are rendered in the game world. We're also going to implement features in an unorthodox order, making some implementations and declarations that won't be completed until we have updated all of our code, this is to avoid jumping back and forth from file to file.

### engine.hpp

There are three changes we need to make to the engine header, first we need to declare a screen width (sW) and screen height (sH) integer to accompany our fovRad. Second, we won't be looking to handle input keys in the main engine anymore, so declare a `TCOD_key_t lastKey;` variable so that we can pass inputs through to the appropriate "player Ai" that we will be creating later. Third, update the player raw pointer to a shared pointer so that we don't have to worry about deleting the player pointer in the engine deconstructor anymore, and so that all of our Entities are a little more uniform in their creation.

Our `Engine();` constructor now becomes `Engine(int sW, int sH);` to accept our new screenwidth and screenheight variables.

### engine.cpp

**Engine::Engine**

Almost everything in this section will change, we'll update the Engine constructor to pass the screenwidth and screenheight integers, our initialisation list will grow to accompany them also, rather than declaring a width of 80 and height of 50 in the `TCODConsole::initRoot` creation we'll pass sW and sH now instead.

We won't be using `emplace_back` to place our player pointer into the Entity vector anymore because we're going to construct our player directly as `player = std::make_shared<Ent>(1,1,'@',"player",TCODColor::white);`. Notice that we've added "player" to the list of arguments our shared Ent pointer takes? We're going to be adding a name field to all Entities from here on out.
We know that we're going to have three new features for Entities, and the player is going to need all three, so add the following three lines to our Engine::Engine constructor to ensure the player is created with the necessary features.

	engine.cpp
			player->mortal = std::make_shared<pcMortal>(30, 2, "your lifeless corpse");
			player->combat = std::make_shared<Combat>(5);
			player->ai = std::make_shared<PlayerAi>();

These pointers should give some idea of the structor we're going to be using. The three parameters for the `<pcMortal>` object are our max health of 30, our defense of 2, and the name of the player corpse. for the `<Combat>` object, we pass our attack value of 5. And for the <PlayerAi> we don't need to pass any values, because the `<PlayerAi>` doesn't need any outside values to construct itself.
			
Finally, to add this "player" Entity to our Entity List we now use `entL.push_back(player);`. The difference from push_back and emplace_back being that push back only inserts pre-made elements in to the vector, emplace_back informs the engine to "create and place" our element, hence why our previous emplace_back command contained a full "std::make_shared<ent>" command within it.

lastly update the dungeon map object to be `(80,45)` so that we have a little room to play with to make a simple gui element.

**void Engine::update()**

Next we need to remove the chaff from the `update()` function as it no longer needs the ix and iy declarations, or to handle the switch cases for key input, or the if condition for when the player has moved and the subsequent player update commands. The update function should retain the check to see if the game has just started up, as well as the gameState switch to IDLE, and the update cycle for `if(gameState==TURN).
We will also still use `TCODSystem::checkForEvent` but because our improved Entity features are going to handle things like updating, we will simply call a `player->update(player);` function to handle updating the player, a method we haven't utilized yet but that implies the player entity itself will handle player movement, in truth, the player entity's AI object will handle the player movement.

**Engine::render()**

Lastly in the engine.cpp, the render function needs to be improved. We still want to clear the root and render the dungeon, but we now want to carry out a series of loops to decide the order in which entities will be rendered.

By the end of this section we expect to have Entities that will be either alive, or dead, and we obviously want the living entities to render on top of the dead ones. My implementation for this is to do the following:

	engine.cpp
		// render an Entity if they are mortal, are dead, and are in the FOV
		for (auto &ent : entL) { if (ent->mortal) { if (ent->mortal->isDead()) { if (dungeon->isInFov(ent->x, ent->y)) { ent->render(); } } } }
		// render an Entity if they are mortal, are not dead, and are in the FOV
		for (auto &ent : entL) { if (ent->mortal) { if (!ent->mortal->isDead()) { if (dungeon->isInFov(ent->x, ent->y)) { ent->render(); } } } }
		// render the player
		player->render();

*NB* : this is not the only way to approach the rendering order, we COULD just move every dead entity to the start of our vector, ensuring that when we do a ranged for, the dead bodies are always rendered first, but I'm not experienced enough to be messing with the order of a vector and if handled incorrectly we might end up accidentally causing a reallocation of memory for the vector. I'm not in control of your code though, if you want to pursue a different method by all means, crack on.

Before we leave the engine.cpp, we want to add one more line to the render function : `TCODConsole::root->print(1, sH - 2, "HP : %d:%d", (int)player->mortal->hp, (int)player->mortal->MaxHp);` if you can't guess what this line does, it will be a stub for our upcoming gui that will print the player current and max health in the bottom left below the map.

## Improved Entities

Now that we've used a bundle of fancy entity features we don't have, it'd probably be a good idea to create them, agreed?

The roguebasin article gives a greater wealth of information on the choices we're about to make, so here is the dystheria short-version. By implementing each potential entity attribute as a shared pointer we give ourselves both a flexible framework by which entities can have many attributes but we also eliminate accidentally bloating entities by adding excessive information to them where it isn't required.

To use an uninspired example, a health potion doesn't need to be sentient or have it's own health pool, so we would waste space and time in creating an Ai procedure for a health potion or giving it a health variable. What's really worth asking is "Why not give health potions their own sentience and health and ability to attack players?" because there is no reason a future magic based enemy wouldn't "animate" items in their laboratory to have them attack the player... that's not something I intend to implement in this tutorial but there is no reason you can't use what you learn here to do just that.

### entity.hpp

We need to expand the Entity class so for our new features, we'll add a variable to hold the name of the entity and add shared pointers for possible entity features, lastly we'll remove the `moveOrAttack` boolean as we will be implementing that in our Ai source as not all Entities will need to move or attack. You should also update the `void update();` function so that it handles a shared pointer to the Entity itself, which we'll refer to as the update `owner` for simplicities sake. It will look something like `void update(std::shared_ptr<Ent> owner);`.

**A note on strings**: We've used the c++ standard string header a little up until this point, and I had intended to try and write this entire tutorial without utilizing archaic or poor practices, but I'm afraid that is a little bit beyond my current skill, so while we could use a `const std::string name;` declaration for the Entity name variable, instead we're going to use `const char *name;` declaration.

### entity.cpp

Update your Ent constructor to include the new name constructor, and update the initialisation list to contain pointers to our 3 entity features, these should be initialised as `nullptr` by default.

Remove the `bool Ent::moveOrAttack` function (or copy it to a notepad later) as we'll reimplement this code in to the ai source later.

Lastly, refactor the update function so that it accepts the `owner` shared pointer, and also change the implementation so that instead of simply calculating if the `owner` is in the FOV and spitting out a message, we check to see if the `owner` has an ai pointer, and if so, it will call the ai->update function that is specific to the `owner` entity.
Confused? Tough. Your implementation should look something like this `void Ent::update(std::shared_ptr<Ent> owner){if(ai)ai->update(owner);}`.

## Goodbye old and busted. Hello new shiny.

The six new source files are up next, the smallest of these is the combat header and implementation, so lets tackle that first.

### combat.hpp

A single class declaration here with the exceptionally creative name of `Combat`, its contents will be three public declarations: a float to handle our attack value that I'm calling `atk`, a constructor that will accept the attack value as an argument `Combat(float atk);`, and a void function named `attack` that takes two arguments in the form of two shared Entity pointers for the attacker and the target... so `void attack(std::shared_ptr<Ent> attacker, std::shared_ptr<Ent> target);`.

### combat.cpp

Include our main header file. Add a straight forward constructor with out single `(float atk)` argument, and we'll initialise the `atk(atk)` by means of initialisation list.

Now the meaty bit of this file, the `Combat::attack` function. Remember to pass both arguments in the declaration (shared pointer for both attacker and target), open the function with a condition test to ensure the target is mortal. If that test returns true, another conditional test to make sure the mortal isn't dead. If *that* test returns true, we'll create a float called `dmg` that is equal to the attack power argument, `atk`, minus the defence of the mortal `target->mortal->def` (don't worry, we implement that in the mortal source), and then we create *another if* to test if dmg is greater than zero.

**IF** it is (a whole lot of ifs going on here) we'll start by spitting out a message to the console in the fashion of "the <attacker->name> attacks the <target->name> for <dmg> damage!". We can do this several ways, the first way that comes to mind for me is to use `std::cout`.

	std::cout << "The " << attacker->name << " attacks the " << target->name << " for " << dmg << " damage!" << std::endl;

Which if we're using C++, is what we ***should*** be using, but unfortunately we want this to be easily convertable for use with TCODConsole later on... so for the moment we have to use `<cstdio>` counterpart which is both relatively archaic and not as typesafe as using `<iostream>` (which std::cout and std::endl belong to.)... anyway, enough grumbling from me.

	printf("The %s hits the %s for %g damage!\n",attacker->name, target->name,dmg);

Now the `printf` implementation might involve 40% less typing, but there are good reasons that it is often passed over in favor of `<iostream>` functions, go google them if you really care.

Anyway, next we need to invoke a function from the target->mortal object that we'll call `takeDamage` and pass the target itself, and the `atk` value. The simplest thing to do here would be to close the innermost if statement, and then open an else statement for the next if condition to say something along the lines of "the attack failed", but you could even put an else statement on the inner condition as well to have greater variance in your output messages.

It's more of a discussion for game design but I'm going to go with the following implementation, and my comments will tell you why. (And yes, I am ending this section with the full source of combat.cpp because I'm a terrible tutorial author.)

	combat.cpp
		#include "main.hpp"
		
		Combat::Combat(float atk):atk(atk){}
		
		void Combat::attack(std::shared_ptr<Ent>attacker,std::shared_ptr<Ent>target){
			if(target->mortal){if(!target->mortal->isDead()){
				float dmg(atk-target->mortal->def);
				if(dmg>0.0){
					printf("The %s hits the %s for %g damage!\n",attacker->name,target->name,dmg);
					// hit successfully does damage
					target->mortal->takeDamage(target,atk);
					// resolve damage
					} else {
					printf("The %s is too weak to harm the %s!\n",attacker->name,target->name);
					// hit does no damage due to not being strong enough
			}}} else {
			printf("The %s is impervious to damage.\n",target->name);
			// The target isn't mortal and has no health, so any attack cannot possibly harm it
		}}

## The mortality of our entities

We've already implied some of the functions and features we're going to need our mortal entities to have, so lets handle the mortal.hpp and mortal.cpp files next.

### mortal.hpp

We'll have three classes here.

**Class 1** : our main `Mortal` class, all of its parameters will be public.

+ Three float values, MaxHp, hp, and def.
+ A `const char *` for corpseName.
+ Mortal constructor with three arguments, the MaxHp, def and corpseName.
+ an inline boolean to check if a mortal `isDead() { return hp <= 0; }`
+ a float function so mortals can takeDamage which will pass a shared entity pointer and float pointer for the owner and dmg respectively.
+ a virtual void function called die which will accept one argument, the shared entity pointer to the owner

**Class 2** : a player character Mortal class, `class pcMortal`, that will be a derived public class of Mortal. Two public declarations, one for the constructor (again, passing the three arguments that the `Mortal` class does), and one for the void die, not virtual this time, and handling the shared Entity pointer for the owner.

**Class 3** : same as Class 2, but for a non-player character, `class npcMortal`.

Nice and relatively painless.

### mortal.cpp

From the top, the usual include to our main header, a constructor for the mortal object that passes our three arguments and then an initialisation list for all four Mortal variables, the initialisation value for each being a self-reference, except hp, hp will be initialised with the MaxHp variable, because all mortals in the game world will be created with full health, obviously.

The `float Mortal::takeDamage` implementation is short and self explanatory:

	mortal.cpp
		float Mortal::takeDamage(std::shared_ptr<Ent>owner,float dmg){
			dmg -= def;
			if (dmg > 0) { hp -= dmg; if (hp <= 0) { die(owner); } } else { dmg = 0; }
			return dmg;
		}

Now, the void for when our mortals die will be responsible for updating the character that represents the dead entity, the colour (i'm from scotland, we speak bastard british) of the character, the name of the entity, and whether it blocks entity movement. You can do evaluate the entire class contents in a single line `owner->ch = '%'; owner->col = TCODColor::darkRed; owner->name = corpseName; owner->blocks = false;`... but maybe don't, because that's ugly. Or do? It's your code.

Next up, pcMortal and npcMortal constructors that look a lot like the Mortal constructor, however, their intialisation lists should read `Mortal(MaxHp,def,corpseName)` because they are `public Mortal` derived classes anyway.

And last, the die voids for pc and npc mortals respectively, these do differ though, because when the player dies we lose the game. They are very simple implementations so again here is my code straight from the source:

	mortal.cpp
		void pcMortal::die(std::shared_ptr<Ent> owner) {
			printf("You have been slain...\n");
			Mortal::die(owner);
			engine.gameState = Engine::LOSE;
		}
		
		void npcMortal::die(std::shared_ptr<Ent> owner) {
			printf("The %s has perished.\n",owner->name);
			Mortal::die(owner);
		}

We now have working mortal entities, isn't that a thing?

## Artificial Ignorance

The last of our three new features is the groundwork for simulated intelligence, we're using "intelligence" really loosely here though.

### ai.hpp

We need an Ai class with a virtual void to update the Entity at whatever our shared pointer for the owner of the Ai may be, you want to initialise the update value to zero in-line. Then create two drived public Ai classes, the PlayerAi and the MobAi, which will be very similar to each other in structure, specifically, we have to overwrite the void update. We will also provide each with a protected `moveOrAttack` functionality, but this will be a bool for the player.

+ The update function overwrites will pass the Ent shared pointer for the Ai owner.
+ The player `moveOrAttack` should be a bool that passes the Ent shared pointer for the Ai owner, an integer for the "tile x" position, `int tx`, and an integer for the "tile y" position, `int ty`.
+ The mob `moveOrAttack` should be a void funciton that passes the same arguments as the player version.
+ lastly, provide the `MobAi` class with a protected integer called "moveCount", which we will use as a means ot counting the number of steps a Mob has taken outside of our FOV.

### ai.cpp

As always, we start by including our main header file, and then the real fun begins...

**Player AI**

*PlayerAi::update*: The PlayerAi update function will now be responsible for handling our input switch case, but the first thing we want to do on it is check if we're dead or not. We could just call a `owner->mortal->isDead()` check, but in the interest of being thorough, we should start by ensuring the player is indeed mortal (because who is to say we won't impart temporary immortality in a future update that allows us to be damaged beyond 0 hit points but still live?).

Next up instantiate two integers, `int ix(0),iy(0);` these will be responsible for accepting our **input x** and **input y** whenever a movement key is pressed. So logically, we now need our switch and cases.

	switch (engine.lastKey.vk) {
	case TCODK_UP: iy = -1; break;
	case TCODK_DOWN: iy = +1; break;
	case TCODK_LEFT: ix = -1; break;
	case TCODK_RIGHT: ix = +1; break;
	default:break;
	}

Lastly, we test for changes to ix and iy, and change the gameState to `TURN` if the test returns true. We're also going to use a test condition to call a moveOrAttack for the player object which if completed succesfully, will update the `engine.dungeon->computeFov()`. Remember, although we haven't declared the implementation yet, we did define in the header that the moveOrAttack function needs three arguments:

	if (ix != 0 || iy != 0) {
		engine.gameState = Engine::TURN;
		if (moveOrAttack(owner, owner->x+ix, owner->y+iy)) { engine.dungeon->computeFov(); }
	}

Easy stuff.

*PlayerAi::moveOrAttack*: this will do a simple "isWall" check, if the tile is a wall, return false, but if the tile isn't a wall we run a *ranged for* iterator against our entity list, using the format we've grown accustomed to of "if they are mortal, check if they are alive and if they are on the tile we want to move to."

If the entity is mortal and alive, we will attack them and return false, if they are not alive, we'll print a message stating that their corpse occupies this tile.

The very end of the boolean function will set the `owner->x = tx;` and `owner->y = ty;` before returning `true`, because the only time we will ever reach the end of the function is in the scenario that we are able to succesfully move to the tile regardless of whether there is a corpse there or not.

**MobAi**

Begin by creating a static const int for `TRACK_TURNS` and initialize it at three, this is going to be the number of turns a Mob will continue to "seek" the player, even after they have gone out of visual range.

*MobAi::update*: The Mob Ai update will start by testing if the entity is mortal and if they are dead, although we can test both of these conditions at the same time using the `&&` operator, I would advise against it, remember when I mentioned animated objects that aren't mortal? if you attempt to test for both mortality AND their `isDead()` status, when it's impossible for them to *have* an `isDead()` status, you'll get a read violation that crashes your game, so don't couple your test conditions when testing "if mortal" **and** "if mortal is dead", do them sequentially instead.

Now, if they ARE mortal and they ARE dead, just return from the update function, because dead things don't move (at least not now).
Next we want to check if the Ai owner is in FOV, and if they are, set the `moveCount` equal to the value of `TRACK_TURNS;`, else set moveCount to reduce by one. `moveCount--;`.

Finally, if the moveCount is greater than 0, call the moveOrAttack function for the Ai owner.

*MobAi::moveOrAttack*:We use a couple of lambdas here to make our life easier, if you want to know more about lambdas go google "c++ lambdas".

Instantiate and initialise five integers, these are dx, dy, sdx, sdy, and distance. We're going to use a bit of math to calculate how far, and in which direction, the Mob should move, sdx and sdy are short for "slide direction x" and "slide direction y" because we're going to use them to allow the Mob to move along walls so that they don't get stuck in a wall-hump when trying to track the player.

	ai.cpp
		void MobAi::moveOrAttack(std::shared_ptr<Ent> owner, int tx, int ty) {
			int dx(tx - owner->x), dy(ty - owner->y), sdx(dx > 0 ? 1:-1), sdy(dy > 0 ? 1:-1), distance((int)sqrt(dx*dx + dy*dy));
			if (distance >= 2) {
				dx = (int)(round(dx / distance)); dy = (int)(round(dy / distance));
				if (engine.dungeon->canWalk(owner->x + dx, owner->y + dy)) { owner->x += dx; owner->y += dy; }
				else if (engine.dungeon->canWalk(owner->x + sdx, owner->y)) { owner->x += sdx; }
				else if (engine.dungeon->canWalk(owner->x, owner->y + sdy)) { owner->y += sdy; }
			}
			else if (owner->combat) { owner->combat->attack(owner, engine.player); }
		}

Take note of the sdx and sdy initialisations, those are the lambdas I mentioned before, a very short test condition presented as an expression that allows us to say "if this is higher than zero return 1 on true and -1 on false" allowing us to later on "push" the Mob with the use of our two else if statements, which basically read "if the Mob *can't* move towards the player, have them slide either vertically by one space, or horizontally by one space".
And obviously, if the distance is less than 2 that means the Mob is standing right next to the player, and it should attempt to attack.

Now, we COULD use a ranged for iterator like we did with the player move or attack Ai, and frankly, that would be the smarter way to do it if we wanted our Mobs to be able to wander the dungeon and fight each other, but these are simple mobs... Dumb mobs... Basic gromlin and hobgobbo idiots that only recognise the player as a threat.

*And with that* all three of our new Entity features are complete. All we need to do now is update the map to reflect these new features.

## Mapping out our new Entities.

Nothing has changed to the class definitions for our Map in this lesson, but some of the implementation needs to be updated.

### map.cpp

Specifically, the `Map::addMonster` function needs to get on board with the new features.

We need to change our current `emplace_back` commands in to a full constructor for an entity with Mortality, Combat capability, and MobAi features. (You have no idea how hard I resisted the urge to type MORTAL KOMBAT AI! And just end the lesson here, full of smug and self-satisfaction.)

Replace the existing `if` and `else` lines with the below...

	map.cpp
			if (mDice < 80) { 
				std::shared_ptr<Ent> gromlin = std::make_shared<Ent>(x, y, 'g', "Gromlin", TCODColor::desaturatedGreen);
				gromlin->mortal = std::make_shared<Mortal>(10, 0, "dead gromlin");
				gromlin->combat = std::make_shared<Combat>(3);
				gromlin->ai = std::make_shared<MobAi>();
				engine.entL.push_back(gromlin); }
			else {
				std::shared_ptr<Ent> hobgobbo = std::make_shared<Ent>(x, y, 'h', "Hobgobbo", TCODColor::darkOrange);
				hobgobbo->mortal = std::make_shared<Mortal>(16, 1, "dead hobgobbo");
				hobgobbo->combat = std::make_shared<Combat>(4);
				hobgobbo->ai = std::make_shared<MobAi>();
				engine.entL.push_back(hobgobbo); }

... to create gromlins that have 10 hp, 0 defense, and become a "dead gromlin" when defeated, and have an attack value of 3. We also create Hobgobbos with 16 health, 1 defense, and an attack value of 4.

This does mean that combat is very very static right now, a flat out numbers game meaning the only real tactics lies in ensuring you get the first hit and have enough health to survive the beating. Don't worry though, we're going to add more groundwork for complexity in future lessons.
