#include "main.hpp"

Engine::Engine() : gameState(START), fovRad(10) {
	TCODConsole::initRoot(80, 50, "ALT|A Libtcod Tutorial v0.5", false);
	entL.emplace_back(std::make_shared<Ent>(1, 1, '@', "player", TCODColor::white)); 
	player = entL[0].get();
	dungeon = std::make_unique<Map>(80, 50);
}

Engine::~Engine() { entL.clear(); }

void Engine::update() {
	TCOD_key_t key;
	if (gameState == START) dungeon->computeFov();
	gameState = IDLE;
	TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS, &key, NULL);
	int ix(0), iy(0);
	switch (key.vk) {
	case TCODK_UP: iy = -1; break;
	case TCODK_DOWN: iy = +1; break;
	case TCODK_LEFT: ix = -1; break;
	case TCODK_RIGHT: ix = +1; break;
	default:break;
	}
	if (ix != 0 || iy != 0) {
		gameState = TURN;
		if (player->moveOrAttack(player->x + ix, player->y + iy)) { dungeon->computeFov(); }
	} 
	if (gameState == TURN) {
		for (auto &ent : entL) { if (ent != entL[0]) { ent->update(); } 
} } }

void Engine::render() { 
	TCODConsole::root->clear(); dungeon->render(); 
	for (auto &ent : entL) { if (dungeon->isInFov(ent->x,ent->y)) { ent->render(); } }
}